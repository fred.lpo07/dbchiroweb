from django.urls import path

from .views import GlobalContribView

app_name = "synthesis"

urlpatterns = [
    # url de recherches de localités
    path("contrib", GlobalContribView.as_view(), name="global_contrib"),
]
