from django.urls import path

from sights.countdetail.views import CountDetailCreate, CountDetailDelete, CountDetailUpdate

urlpatterns = [
    path(
        "sighting/<int:pk>/countdetail/add",
        CountDetailCreate.as_view(),
        name="countdetail_create",
    ),
    path(
        "countdetail/<int:pk>/update",
        CountDetailUpdate.as_view(),
        name="countdetail_update",
    ),
    path(
        "countdetail/<int:pk>/delete",
        CountDetailDelete.as_view(),
        name="countdetail_delete",
    ),
]
