from django.urls import path

from sights.metaplace.api import GeoJSONBBoxFilteredMetaPlaces, GeoJSONMetaPlaces
from sights.metaplace.views import (
    MetaPlaceCreate,
    MetaPlaceDelete,
    MetaPlaceDetail,
    MetaPlaceList,
    MetaPlaceUpdate,
)

urlpatterns = [
    path("metaplace/add", MetaPlaceCreate.as_view(), name="metaplace_create"),
    path("metaplace/search", MetaPlaceList.as_view(), name="metaplace_search"),
    path("metaplace/<int:pk>/detail", MetaPlaceDetail.as_view(), name="metaplace_detail"),
    path("metaplace/<int:pk>/update", MetaPlaceUpdate.as_view(), name="metaplace_update"),
    path("metaplace/<int:pk>/delete", MetaPlaceDelete.as_view(), name="metaplace_delete"),
    path("metaplace/api/search", GeoJSONMetaPlaces.as_view(), name="metaplace_search_api"),
    path(
        "metaplace/bbox/api/search",
        GeoJSONBBoxFilteredMetaPlaces.as_view(),
        name="metaplace_bbox_search_api",
    ),
    path(
        "metaplace/api/search",
        GeoJSONMetaPlaces.as_view(),
        name="metaplace_search_api",
    ),
]
