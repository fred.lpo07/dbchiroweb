# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from rest_framework import generics
from rest_framework_gis.filters import InBBoxFilter

from sights.mixins import LargeGeoJsonPageNumberPagination
from sights.models import MetaPlace
from sights.place.serializers import CountModelMixin, MetaPlaceSerializer

logger = logging.getLogger(__name__)


class GeoJSONMetaPlaces(LoginRequiredMixin, CountModelMixin, generics.ListAPIView):
    serializer_class = MetaPlaceSerializer
    pagination_class = LargeGeoJsonPageNumberPagination

    def get_queryset(self):
        name = self.request.query_params.get("name", None)
        type = self.request.query_params.get("type", None)
        creator = self.request.query_params.get("creator", None)

        qs = MetaPlace.objects.order_by("-timestamp_create")

        if name is not None:
            qs = qs.filter(name__icontains=name)
        if type is not None:
            qs = qs.filter(
                Q(type__code__icontains=type)
                | Q(type__category__icontains=type)
                | Q(type__descr__icontains=type)
            )
        if creator is not None:
            qs = qs.filter(creator__username__icontains=creator)

        if creator is not None:
            qs = qs.filter(
                Q(created_by__first_name__icontains=creator)
                | Q(created_by__last_name__icontains=creator)
                | Q(created_by__username__icontains=creator)
            )

        logger.debug(qs.query)
        qs = qs.select_related("created_by", "type").prefetch_related()
        return qs.all()


class GeoJSONBBoxFilteredMetaPlaces(LoginRequiredMixin, CountModelMixin, generics.ListAPIView):
    serializer_class = MetaPlaceSerializer
    pagination_class = LargeGeoJsonPageNumberPagination
    bbox_filter_field = "geom"
    filter_backends = (InBBoxFilter,)
    bbox_filter_include_overlapping = True  # Optional

    def get_queryset(self):
        name = self.request.query_params.get("name", None)
        type = self.request.query_params.get("type", None)
        creator = self.request.query_params.get("creator", None)

        qs = MetaPlace.objects.order_by("-timestamp_create")

        if name is not None:
            qs = qs.filter(name__icontains=name)
        if type is not None:
            qs = qs.filter(
                Q(type__code__icontains=type)
                | Q(type__category__icontains=type)
                | Q(type__descr__icontains=type)
            )
        if creator is not None:
            qs = qs.filter(creator__username__icontains=creator)

        if creator is not None:
            qs = qs.filter(
                Q(created_by__first_name__icontains=creator)
                | Q(created_by__last_name__icontains=creator)
                | Q(created_by__username__icontains=creator)
            )

        logger.debug(qs.query)
        qs = qs.select_related("created_by", "type").prefetch_related()
        return qs.all()
