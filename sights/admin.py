import nested_admin
from django.contrib import admin
from leaflet.admin import LeafletGeoAdminMixin

from .models import (
    Bridge,
    Build,
    Cave,
    CountDetail,
    Place,
    Session,
    Sighting,
    Study,
    Transmitter,
    Tree,
)


class CountDetailInLine(nested_admin.NestedStackedInline):
    model = CountDetail
    extra = 0
    fk_name = "sighting"
    readonly_fields = ("etat_sexuel",)
    fieldsets = (
        (None, {"fields": (("sex", "age"), ("precision", "count", "unit"))}),
        (
            "Informations complémentaires",
            {
                "classes": ("grp-collapse grp-open",),
                "fields": (
                    ("time", "manipulator", "validator", "transmitter"),
                    ("comment", "etat_sexuel"),
                ),
            },
        ),
        (
            "Données biométriques",
            {
                "classes": (
                    "collapse",
                    "grp-collapse grp-open",
                ),
                "fields": (
                    ("ab", "d5", "d3", "pouce"),
                    ("queue", "tibia", "cm3", "tragus"),
                    ("poids", "testicule", "epididyme", "tuniq_vag"),
                    ("gland_taille", "gland_coul", "mamelle", "gestation"),
                    ("epiphyse", "chinspot", "usure_dent"),
                ),
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        qs = super(CountDetailInLine, self).get_queryset(request)
        if request.user.is_staff and request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)


class SightingInLine(nested_admin.NestedTabularInline):
    model = Sighting
    extra = 0
    inlines = [
        CountDetailInLine,
    ]

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        qs = super(SightingInLine, self).get_queryset(request)
        if request.user.is_staff and request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)


class SightingAdmin(nested_admin.NestedModelAdmin):
    inlines = [
        CountDetailInLine,
    ]

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        qs = super(SightingAdmin, self).get_queryset(request)
        if request.user.is_staff and request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)


class SessionInLine(nested_admin.NestedStackedInline):
    model = Session
    extra = 0
    inlines = [
        SightingInLine,
    ]
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "place",
                    "method",
                    "date_start",
                    "date_end",
                    "main_observer",
                )
            },
        ),
        (
            "Commentaire",
            {
                "classes": (
                    "collapse",
                    "grp-collapse grp-open",
                ),
                "fields": ("comment",),
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()


class SessionAdmin(nested_admin.NestedModelAdmin):
    inlines = [
        SightingInLine,
    ]
    fieldsets = (
        (
            None,
            {
                "fields": (
                    ("name", "place"),
                    ("method", "main_observer"),
                    ("date_start", "date_end"),
                )
            },
        ),
        (
            "Commentaire",
            {
                "classes": (
                    "collapse",
                    "grp-collapse grp-open",
                ),
                "fields": ("comment",),
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceBridgeDetailInline(nested_admin.NestedStackedInline):
    model = Bridge
    extra = 0
    can_delete = False
    verbose_name_plural = "Détails du pont"

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceBuildDetailInline(nested_admin.NestedStackedInline):
    model = Build
    extra = 0
    can_delete = False
    verbose_name_plural = "Détails du bâtiment"

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceTreeDetailInline(nested_admin.NestedStackedInline):
    model = Tree
    extra = 0
    can_delete = False
    verbose_name_plural = "Détails d'un arbre"

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceCaveDetailInline(LeafletGeoAdminMixin, nested_admin.NestedStackedInline):
    model = Cave
    extra = 0
    can_delete = False
    verbose_name_plural = "Détails d'une cavité"

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()


class PlaceAdmin(LeafletGeoAdminMixin, nested_admin.NestedModelAdmin):
    model = Place
    inlines = [
        PlaceBridgeDetailInline,
        PlaceBuildDetailInline,
        PlaceTreeDetailInline,
        PlaceCaveDetailInline,
        SessionInLine,
    ]
    list_filter = ("territory", ("created_by", admin.RelatedOnlyFieldListFilter))
    list_display = [
        "name",
        "territory",
        "timestamp_create",
        "timestamp_update",
        "is_hidden",
        "type",
        "is_gite",
        "convention",
        "convention_file",
        "photo_file",
        "plan_localite",
    ]
    search_fields = ["name", "created_by"]
    sortable_field_name = [
        "name",
        "municipality",
        "territory" "timestamp_create",
        "timestamp_update",
    ]
    autocomplete_lookup_fields = {
        "name": ["name"],
    }
    readonly_fields = (
        "x",
        "y",
    )
    fieldsets = (
        (
            None,
            {
                "fields": (
                    ("name", "precision"),
                    ("is_hidden", "type", "domain"),
                    "geom",
                )
            },
        ),
        (
            "Gites et refuges",
            {
                "classes": (
                    "collapse",
                    "grp-collapse grp-open",
                ),
                "fields": (
                    ("proprietary", "is_gite"),
                    ("convention", "convention_file"),
                ),
            },
        ),
        (
            "Autres informations",
            {
                "classes": (
                    "collapse",
                    "grp-collapse grp-open",
                ),
                "fields": (
                    ("bdsource", "id_bdsource", "territory"),
                    ("photo_file", "id_bdcavite", "plan_localite"),
                ),
            },
        ),
        (
            "Commentaire",
            {
                "classes": (
                    "collapse",
                    "grp-collapse grp-open",
                ),
                "fields": ("comment",),
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()

    def get_formsets(self, request, obj=type):
        for inline in self.get_inline_instances(request, obj):
            if isinstance(inline, PlaceBridgeDetailInline) and obj == "pont":
                continue
            yield inline.get_formset(request, obj)


class ActuAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "timestamp_create", "timestamp_update")
    fieldsets = [
        (None, {"fields": ("title", "briefdescr", "text")}),
    ]

    def save_model(self, request, obj, form, change):
        if getattr(obj, "author", None) is None:
            obj.author = request.user
        obj.save()


admin.site.register(Place, PlaceAdmin)
admin.site.register(Sighting, SightingAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(Transmitter)
admin.site.register(Study)
