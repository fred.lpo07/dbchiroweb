# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Prefetch
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

# from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, ModelViewSet, mixins

from sights.mixins import LargeGeoJsonPageNumberPagination, SightingFilteredListWithPermissions
from sights.models import Sighting
from sights.observation.serializers import (
    EditSightingSerializer,
    EditSightingSerializerTest,
    GeoSightingSerializer,
)

from ..mixins import SightingListPermissionsMixin
from .permissions import SightingEditPermission

logger = logging.getLogger(__name__)


class GeoJSONSighting(
    LoginRequiredMixin,
    SightingFilteredListWithPermissions,
    ListAPIView,
):
    queryset = (
        Sighting.objects.select_related("created_by")
        .select_related("updated_by")
        .select_related("observer")
        .select_related("session")
        .select_related("codesp")
        .prefetch_related("session__place")
        .prefetch_related("session__place__municipality")
        .prefetch_related("session__place__territory")
        .prefetch_related("session__place__type")
        .prefetch_related("session__contact")
        .prefetch_related("session__main_observer")
        .prefetch_related("session__other_observer")
        .prefetch_related("session__study")
        .all()
    )
    serializer_class = GeoSightingSerializer
    pagination_class = LargeGeoJsonPageNumberPagination

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()

        return qs.distinct().order_by("-timestamp_update")


class EditSighting(SightingListPermissionsMixin, ModelViewSet):
    serializer_class = EditSightingSerializerTest
    permission_classes = [
        IsAuthenticated,
        SightingEditPermission,
    ]

    queryset = Sighting.objects.select_related("session").prefetch_related("countdetails").all()

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        if self.action in ("list", "create"):
            session = self.kwargs["session"]
            qs = qs.filter(session=session)
        else:
            qs = qs.all()
        return qs

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class DeleteSighting(mixins.DestroyModelMixin, GenericViewSet):
    serializer_class = EditSightingSerializer
    queryset = Sighting.objects.all()
    permission_classes = [
        IsAuthenticated,
        SightingEditPermission,
    ]
