# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework import generics
from rest_framework_gis.filters import InBBoxFilter

from sights.mixins import LargeGeoJsonPageNumberPagination, PlaceFilteredListWithPermissions
from sights.models import Place
from sights.place.serializers import CountModelMixin, PlaceSerializer

# Get an instance of a logger
logger = logging.getLogger(__name__)

place_qs = (
    (
        Place.objects.select_related("municipality")
        .select_related("metaplace")
        .select_related("created_by")
        .select_related("type")
        .select_related("territory")
    )
    .order_by("-timestamp_update")
    .all()
)


class GeoJSONPlaces(
    LoginRequiredMixin,
    PlaceFilteredListWithPermissions,
    CountModelMixin,
    generics.ListAPIView,
):
    serializer_class = PlaceSerializer
    pagination_class = LargeGeoJsonPageNumberPagination
    queryset = place_qs


class GeoJSONBBoxFilteredPlaces(
    LoginRequiredMixin,
    PlaceFilteredListWithPermissions,
    CountModelMixin,
    generics.ListAPIView,
):
    serializer_class = PlaceSerializer
    bbox_filter_field = "geom"
    filter_backends = (InBBoxFilter,)
    bbox_filter_include_overlapping = True  # Optional
    queryset = place_qs
