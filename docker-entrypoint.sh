#!/bin/bash

echo ""
echo "dbChiro init_____________________________________________"
echo ""
echo "              *         *      *         *"
echo "          ***          **********          ***"
echo "       *****           **********           *****"
echo "     *******           **********           *******"
echo "   **********         ************         **********"
echo "  ****************************************************"
echo " ******************************************************"
echo "********************************************************"
echo "********************************************************"
echo "********************************************************"
echo " ******************************************************"
echo "  ********      ************************      ********"
echo "   *******       *     *********      *       *******"
echo "     ******             *******              ******"
echo "       *****             *****              *****"
echo "          ***             ***              ***"
echo "            **             *              **"
echo ""

export DBHOST=${POSTGRES_HOST:-db}
export DBNAME=${POSTGRES_DB:-dbchiro}
export DBUSER=${POSTGRES_USER:-dbadmin}
export DBPWD=${POSTGRES_PASSWORD:-myDbPassword}
export DBPORT=${POSTGRES_PORT:-5432}
export SECRETKEY=${SECRETKEY:-$(tr </dev/urandom -dc _A-Z-a-z-0-9 | head -c50)}
export SRID=${SRID:-4326}
export X=${DEFAULT_X:-5}
export Y=${DEFAULT_Y:-45}
export SETTINGS=${SETTINGS:-production}

export SU_USERNAME=${SU_USERNAME:-admin}
export SU_PWD=${SU_PWD:-admin}
export SU_EMAIL=${SU_EMAIL:-admin@domain.tld}

export EMAIL_SMTP=${EMAIL_SMTP:-smtp.domain.tld}
export EMAIL_SSL=${EMAIL_SSL:-False}
export EMAIL_PORT=${EMAIL_PORT:-465}
export EMAIL_USER=${EMAIL_USER:-$SU_EMAIL}
export EMAIL_PWD=${EMAIL_PWD:-passWord}
export EMAIL_DEFAULT_FROM=${EMAIL_SMTP:-$EMAIL_USER}

export IGN_API_KEY=${IGN_API_KEY:-yourapikey}

shopt -s dotglob nullglob
mv /usr/src/dbchiro/* /app/

if [ ! -f /dbchiro/config.py ]; then
    echo "generate new config file"
    tree /app/dbchiro/
    mv /app/dbchiro/settings/configuration/config.py.sample /dbchiro/config.py
    sed -i "s/dbHost/${DBHOST}/g" /dbchiro/config.py
    sed -i "s/dbName/${DBNAME}/g" /dbchiro/config.py
    sed -i "s/dbUser/${DBUSER}/g" /dbchiro/config.py
    sed -i "s/dbPassword/${DBPWD}/g" /dbchiro/config.py
    sed -i "s/dbPort/${DBPORT}/g" /dbchiro/config.py
    sed -i "s/secretKey/${SECRETKEY}/g" /dbchiro/config.py
    sed -i "s/geoDefaultProj/${SRID}/g" /dbchiro/config.py
    sed -i "s/geoDefaultX/${X}/g" /dbchiro/config.py
    sed -i "s/geoDefaultY/${Y}/g" /dbchiro/config.py
    sed -i "s/emailSmtpHost/${EMAIL_SMTP}/g" /dbchiro/config.py
    sed -i "s/emailSsl/${EMAIL_SSL}/g" /dbchiro/config.py
    sed -i "s/emailPort/${EMAIL_PORT}/g" /dbchiro/config.py
    sed -i "s/emailAddress/${EMAIL_USER}/g" /dbchiro/config.py
    sed -i "s/emailPassword/${EMAIL_PWD}/g" /dbchiro/config.py
    sed -i "s/emailDefaultFrom/${EMAIL_DEFAULT_FROM}/g" /dbchiro/config.py
    sed -i "s/IGN_API_KEY = 'yourapikey'/IGN_API_KEY = '${IGN_API_KEY}'/g" /dbchiro/config.py
else
    echo "config file already exists"
fi
rm -f /app/dbchiro/settings/configuration/config.py
ln -s /dbchiro/config.py /app/dbchiro/settings/configuration/config.py

_djapps="accounts blog dicts geodata management sights"
if [ ! -d /dbchiro/migrations ]; then
    echo "Create symlink for migrations dirs"
    mkdir /dbchiro/migrations
    for _a in $_djapps; do
        echo "create $a migrations symlink"
        if [ ! -d /dbchiro/migrations/$_a ]; then
            mkdir /dbchiro/migrations/$_a
        else
            "$a migration dir and symlink already exists"
        fi
    done
else
    echo "Migrations dir already exists"
fi

for a in $_djapps; do
    rm -rf /app/$_a/migrations
    ln -s /dbchiro/migrations/$_a /app/$_a/migrations
done

if [ ! -d /dbchiro/media ]; then
    echo "Create media dir"
    mkdir /dbchiro/media
else
    echo "Media dir already exusts"
fi

rm -rf /app/media
ln -s /dbchiro/media /app/media

until pg_isready -h $DBHOST -p $DBPORT; do
    echo "Awaiting Database container on ${DBHOST}:${DBPORT}"
    sleep 1
done
sleep 2

cd /app

chown -R www-data:www-data /app

export DJANGO_SETTINGS_MODULE=dbchiro.settings.$SETTINGS

echo "************ Init database ****************"
# poetry shell
python3 -m manage makemigrations accounts core blog dicts geodata management sights
python3 -m manage migrate
python3 -m manage collectstatic --noinput

if [ ! -f /dbchiro/logo_site.png ]; then
    echo "Create default site logo image"
    cp /app/static/img/logo_site.png.sample /dbchiro/logo_site.png
else
    echo "Site logo already exists"
fi
rm -f /app/static/img/logo_site.png
ln -s /dbchiro/logo_site.png /app/static/img/logo_site.png

echo "************** Create dbChiro SU ******************"

script="
from accounts.models import Profile;

username = '$SU_USERNAME';
password = '$SU_PWD';
email = '$SU_EMAIL';

if Profile.objects.filter(is_superuser=True).count()==0:
    superuser=Profile.objects.create_user(username, email, password);
    superuser.is_superuser=True;
    superuser.save();
    print('Superuser',username,'created.');
else:
    print('One or more Superuser already exists, creation skipped.')
"
printf "$script" | python3 manage.py shell

echo "************** Populate with initial data ***************"

export countdict=$(printf "from dicts.models import PlacePrecision; print(PlacePrecision.objects.count())" | python3 manage.py shell)

if [ $countdict -eq 0 ]; then
    echo "load dicts data"
    python3 -m manage loaddata dicts/fixtures/dicts.xml
    rm -f dicts/fixtures/dicts.xml
    echo "dicts data loaded"
else
    echo "dicts data already exists"
fi

export countterritory=$(printf "from geodata.models import Territory; print(Territory.objects.count())" | python3 manage.py shell)

if [ $countterritory -eq 0 ]; then
    echo "load geo datas, this may take a while..."
    python3 -m manage loaddata geodata/fixtures/geodata.xml
    rm -f geodata/fixtures/geodata.xml
    echo "geo datas loaded"
else
    echo "geo datas already exists"
fi

#nginx Conf
echo "************ Configure NGINX ***************"
rm -f /etc/nginx/conf.d/default.conf
cp /app/dbchiro/settings/configuration/nginx_dbchiro.conf.sample /etc/nginx/conf.d/dbchiro.conf
rm -rf /etc/nginx/sites-enabled/*
sed -i "s/pathToApp/app/g" /etc/nginx/conf.d/dbchiro.conf
#/etc/init.d/nginx restart
nginx -g "daemon off;" &

echo "************ Start Gunicorn ***************"
cd /app
poetry shell
gunicorn -b 127.0.0.1:8000 dbchiro.wsgi
