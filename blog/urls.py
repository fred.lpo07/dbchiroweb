from django.urls import path

from .views import ActuCreate, ActuDelete, ActuDetail, ActuList, ActuUpdate

app_name = "blog"

urlpatterns = [
    path("", ActuList.as_view(), name="home"),
    path("actu/list/page<int:page>", ActuList.as_view(), name="home"),
    path("actu/<int:pk>/detail", ActuDetail.as_view(), name="actu_detail"),
    path("actu/add", ActuCreate.as_view(), name="actu_create"),
    path("actu/<int:pk>/update", ActuUpdate.as_view(), name="actu_update"),
    path("actu/<int:pk>/delete", ActuDelete.as_view(), name="actu_delete"),
]
