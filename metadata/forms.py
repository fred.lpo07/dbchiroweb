from crispy_forms.bootstrap import Accordion
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Button, Column, Layout, Row, Submit
from django import forms
from django.utils.translation import ugettext_lazy as _

from core.forms import InfoAccordionGroup, PrimaryAccordionGroup
from metadata.models import AcquisitionFramework, Organism


class OrganismForm(forms.ModelForm):
    class Meta:
        model = Organism
        fields = (
            "id_organism",
            "label",
            "short_label",
            "action_scope",
            "geographic_area",
            "geographic_area_details",
            "status",
            "type",
            "address",
            "postal_code",
            "municipality",
            "email",
            "phone_number",
            "url",
        )

    widgets = {
        "id_organism": forms.HiddenInput(),
    }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"

        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "gotoCount",
                        _("Enregistrer et ajouter des membres"),
                        css_class="btn btn-success btn-sm",
                    ),
                    Submit(
                        "_addanother",
                        _("Enregistrer et ajouter un autre organisme"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button("cancel", "Annuler", css_class="btn-warning"),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Description",
                    Row(
                        Column("label", css_class="col-lg-6 col-sm-12"),
                        Column("short_label", css_class="col-lg-6 col-sm-12"),
                        Column("action_scope", css_class="col-lg-6 col-sm-12"),
                        Column("geographic_area", css_class="col-lg-6 col-sm-12"),
                        Column("geographic_area_details", css_class="col-lg-12"),
                        Column("status", css_class="col-lg-6 col-sm-12"),
                        Column("type", css_class="col-lg-6 col-sm-12"),
                    ),
                ),
                InfoAccordionGroup(
                    "Coordonnées",
                    Row(
                        Column("email", css_class="col-md-6 col-sm-12"),
                        Column("phone_number", css_class="col-md-6 col-sm-12"),
                        Column("url", css_class="col-lg-12"),
                        Column("address", css_class="col-md-4 col-sm-12"),
                        Column("postal_code", css_class="col-md-4 col-sm-12"),
                        Column("municipality", css_class="col-md-4 col-sm-12"),
                    ),
                ),
            ),
            Row(
                Column(
                    Submit(
                        "gotoCount",
                        _("Enregistrer et ajouter des détails"),
                        css_class="btn btn-success btn-sm",
                    ),
                    Submit(
                        "_addanother",
                        _("Enregistrer et ajouter une autre observation"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
        )

        super(OrganismForm, self).__init__(*args, **kwargs)


class AcquisitionFrameworkForm(forms.ModelForm):
    class Meta:
        model = AcquisitionFramework
        fields = (
            "id_acquisition_framework",
            "label",
            "desc",
            "context",
            "objective",
            "territory_level",
            "territory",
            "geo_accuracy",
            "keywords",
            "financing_type",
            "actor",
            "target_description",
            "is_metaframework",
            "parent_framework",
            "ecologic_or_geologic_target",
            "date_start",
            "date_end",
        )

    widgets = {
        "id_acquisition_framework": forms.HiddenInput(),
    }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"

        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "gotoCount",
                        _("Enregistrer et ajouter des membres"),
                        css_class="btn btn-success btn-sm",
                    ),
                    Submit(
                        "_addanother",
                        _("Enregistrer et ajouter un autre organisme"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button("cancel", "Annuler", css_class="btn-warning"),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Description",
                    Row(
                        Column("label", css_class="col-lg-12"),
                        Column("desc", css_class="col-lg-12"),
                        Column("context", css_class="col-lg-6 col-sm-12"),
                        Column("objective", css_class="col-lg-6 col-sm-12"),
                        Column("territory_level", css_class="col-lg-6 col-sm-12"),
                        Column("geo_accuracy", css_class="col-lg-6 col-sm-12"),
                        Column("territory", css_class="col-lg-12"),
                        Column("keywords", css_class="col-lg-4 col-sm-12"),
                        Column("financing_type", css_class="col-lg-4 col-sm-12"),
                        Column("actor", css_class="col-lg-4 col-sm-12"),
                        Column("target_description", css_class="col-lg-12"),
                        Column("is_metaframework", css_class="col-lg-6 col-sm-12"),
                        Column("parent_framework", css_class="col-lg-6 col-sm-12"),
                        Column(
                            "ecologic_or_geologic_target",
                            css_class="col-lg-12",
                        ),
                        Column("date_start", css_class="col-lg-6 col-sm-12"),
                        Column("date_end", css_class="col-lg-6 col-sm-12"),
                    ),
                ),
            ),
            Row(
                Column(
                    Submit(
                        "gotoCount",
                        _("Enregistrer et ajouter des détails"),
                        css_class="btn btn-success btn-sm",
                    ),
                    Submit(
                        "_addanother",
                        _("Enregistrer et ajouter une autre observation"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
        )

        super(AcquisitionFrameworkForm, self).__init__(*args, **kwargs)
