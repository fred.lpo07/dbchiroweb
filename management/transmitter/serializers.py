from rest_framework.serializers import ModelSerializer

from ..models import Transmitter


class SearchTransmitterSerializer(ModelSerializer):
    class Meta:
        model = Transmitter
        fields = (
            "id_transmitter",
            "reference",
            "name",
            "status",
        )
