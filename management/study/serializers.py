import logging

from rest_framework.serializers import ModelSerializer
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from accounts.serializer import ProfileSearchSerializer
from sights.models import Place, Session, Sighting
from sights.observation.serializers import ContactSerializer

from ..models import Study

logger = logging.getLogger(__name__)


class SightingSerializer(ModelSerializer):
    class Meta:
        model = Sighting
        depth = 1
        fields = (
            "id_sighting",
            "codesp",
            "breed_colo",
            "total_count",
            "period",
            "is_doubtful",
            "comment",
            "timestamp_create",
        )


class SessionSerializer(ModelSerializer):
    contact = ContactSerializer()
    observations = SightingSerializer(many=True)
    main_observer = ProfileSearchSerializer()

    class Meta:
        model = Session
        depth = 0
        fields = ("id_session", "name", "contact", "date_start", "observations", "main_observer")

    def get_observations(self, session):
        observations = Sighting.objects.filter(session=session.pk).all()
        return observations


class StudyDataSerializer(GeoFeatureModelSerializer):
    sessions = SessionSerializer(source="study_sessions", many=True, read_only=True)

    class Meta:
        model = Place
        geo_field = "geom"
        depth = 0
        fields = ("name", "id_place", "sessions")

    # def get_sessions(self, instance):
    #     queryset = instance.sessions.order_by("-date_start")
    #     return SessionSerializer(queryset, many=True).data


class StudySerializer(ModelSerializer):
    project_manager = ProfileSearchSerializer()
    updated_by = ProfileSearchSerializer()
    created_by = ProfileSearchSerializer()

    class Meta:
        model = Study
        depth = 1
        fields = "__all__"
