"""
    Vues de l'application Sights
"""


from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from ..forms import StudyForm
from ..mixins import ManagementAuthMixin
from ..models import Study
from .mixins import StudyAuthMixin


class StudyCreate(LoginRequiredMixin, CreateView):
    """Create view for the Study model."""

    model = Study
    form_class = StudyForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(StudyCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(StudyCreate, self).get_context_data(**kwargs)
        context["icon"] = "fi-page"
        context["title"] = _("Ajout d'une étude")
        context[
            "js"
        ] = """
        """
        return context


class StudyUpdate(ManagementAuthMixin, UpdateView):
    model = Study
    form_class = StudyForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(StudyUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(StudyUpdate, self).get_context_data(**kwargs)
        context["icon"] = "fi-page"
        context["title"] = _("Ajout d'une étude")
        context[
            "js"
        ] = """
        """
        return context


class StudyDelete(ManagementAuthMixin, DeleteView):
    model = Study
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("management:study_list")

    def get_context_data(self, **kwargs):
        context = super(StudyDelete, self).get_context_data(**kwargs)
        context["icon"] = "fi-trash"
        context["title"] = _("Suppression d'une étude")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer l'étude")
        return context


class StudyList(LoginRequiredMixin, TemplateView):
    template_name = "study_list.html"

    def get_context_data(self, **kwargs):
        context = super(StudyList, self).get_context_data(**kwargs)
        context["icon"] = "fi-page"
        context["title"] = _("Liste des études")
        context[
            "js"
        ] = """
        """
        return context


class StudyDetail(LoginRequiredMixin, StudyAuthMixin, DetailView):
    model = Study
    template_name = "study_detail.html"

    def get_context_data(self, **kwargs):
        context = super(StudyDetail, self).get_context_data(**kwargs)
        context["icon"] = "fi-page"
        context["title"] = _("Etude")

        return context
