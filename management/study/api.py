# import the logging library
import logging

from django.db.models import Prefetch, Q
from rest_framework.generics import GenericAPIView, ListAPIView

from sights.models import Place, Session

from ..models import Study
from .permissions import StudyDetailPermission
from .serializers import StudyDataSerializer, StudySerializer

logger = logging.getLogger(__name__)


class StudyDatasApi(ListAPIView):
    serializer_class = StudyDataSerializer
    permission_class = StudyDetailPermission

    def get_queryset(self, *args, **kwargs):
        # qs = super().get_queryset()7
        pk = self.kwargs.get("pk")
        qs = (
            Place.objects.select_related("created_by")
            .select_related("updated_by")
            .prefetch_related(
                Prefetch(
                    "sessions",
                    queryset=Session.objects.filter(study=pk),
                    to_attr="study_sessions",
                )
            )
            .prefetch_related("study_sessions__study")
            .prefetch_related(Prefetch("study_sessions__observations"))
            .prefetch_related(Prefetch("study_sessions__contact"))
            .prefetch_related(Prefetch("study_sessions__observations__codesp"))
            .prefetch_related(Prefetch("study_sessions__main_observer"))
            # .prefetch_related(Prefetch("sessions__other_observer"))
        )
        logger.debug(f"pk is {pk}")
        study = Study.objects.get(pk=pk)
        logger.debug(f"study is {study}")
        # user = self.request.user
        # if study.project_manager != self.request.user.pk
        qs = qs.filter(sessions__study=pk)
        logger.debug(f"QS TEST {qs.query}")
        return qs.distinct().order_by("-timestamp_update").all()


class StudyListApi(ListAPIView):
    serializer_class = StudySerializer
    queryset = (
        Study.objects.select_related("created_by")
        .select_related("updated_by")
        .select_related("project_manager")
        .prefetch_related("sessions")
        .order_by("timestamp_update")
        # .prefetch_related(Prefetch("sessions__other_observer"))
    )

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        user = self.request.user
        if user.access_all_data or user.edit_all_data or user.is_superuser:
            # If advanced user, can see all studies
            qs = qs
        if not (user.access_all_data or user.edit_all_data or user.is_superuser):
            # If not advanced user, can see own and public studies
            logger.debug("step1")
            logger.debug(f"QS1 {qs}")
            qs = qs.filter(
                (Q(confidential=True) & Q(project_manager=user))
                | (Q(confidential=True) & Q(created_by=user))
                | Q(confidential=False)
            )
            logger.debug(f"QS2 {qs}")
        return qs
