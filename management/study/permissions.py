from rest_framework.permissions import BasePermission


class StudyDetailPermission(BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj) -> bool:
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.user:
            loggeduser = request.user
        # Instance must have an attribute named `owner`.
        return (
            loggeduser.edit_all_data
            or loggeduser.access_all_data
            or loggeduser.is_resp
            or loggeduser.is_staff
            or loggeduser.is_superuser
            or obj.project_manager == loggeduser
        )
