FROM python:3.8-slim

ARG DEBIAN_FRONTEND=noninteractive
ARG APPDIR=/usr/src/dbchiro
# ARG USERNAME=dbchiro
ARG DJANGO_ENV

ENV DJANGO_ENV=${DJANGO_ENV} \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VIRTUALENVS_CREATE=false \
  POETRY_CACHE_DIR='/var/cache/pypoetry'


RUN apt-get update -y \
     && apt-get install --no-install-recommends -y \
     libpq-dev nginx \
     binutils libproj-dev gdal-bin postgresql-client \
     &&  apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/* \
  && pip install --upgrade pip && pip install poetry && poetry --version

WORKDIR ${APPDIR}

COPY pyproject.toml poetry.lock ./

RUN poetry install

# RUN addgroup --system -gid 1001 $USERNAME && \
#     adduser --system --home $USERDIR --uid 1001 --group $USERNAME  && \
#     chown -R $USERNAME:$USERNAME  $USERDIR && \
#     mkdir /app && chown -R $USERNAME:$USERNAME /app && \
#     mkdir -p ${POETRY_CACHE_DIR} && chown -R $USERNAME:$USERNAME ${POETRY_CACHE_DIR}




COPY docker-entrypoint.sh /usr/bin/docker-entrypoint.sh

COPY . ${APPDIR}

VOLUME ["/dbchiro"]

WORKDIR /app

EXPOSE 80

ENTRYPOINT ["bash", "/usr/bin/docker-entrypoint.sh"]
