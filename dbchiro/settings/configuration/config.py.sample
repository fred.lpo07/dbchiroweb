"""FICHIER DE CONFIGURATION DE DBCHIROWEB"""


# Nom du site, affiché sur les pages internet.
SITE_NAME = "dbChiro[web]"

#  Hôtes de cette instance de dbChiro (liste python, entre crochets), si plusieurs, ajouter comme tel ['domaine1.org','domaine2.net','.domaine3.net']
# https://docs.djangoproject.com/fr/1.11/ref/settings/#std:setting-ALLOWED_HOSTS
CUSTOM_HOSTS = ["*"]

# Clé secrète, peut être générée ici :
# https://www.miniwebtool.com/django-secret-key-generator/
SECRET_KEY = "secretKey"

# Paramètres de la base de donnée PostgreSQL, l'utilisateur doit être superutilisateur
DB_HOST = "dbHost"  # Hôte de la bdd
DB_PORT = dbPort  # Port de la bdd
DB_NAME = "dbName"  # Nom de la bdd
DB_USER = "dbUser"  # utilisateur de la bdd
DB_PWD = "dbPassword"  # Mot de passe de la bdd

# Paramètres du compte email pour la transmission d'emails
EMAIL_SMTP = "smtpHost"  # pour ovh
EMAIL_SSL = True  # pour ovh
EMAIL_PORT = 465  # pour ovh
EMAIL_USER = "webadminEmailAddress"
EMAIL_PWD = "webadminEmailPwd"
EMAIL_DEFAULT_FROM = "webadminEmailDefaultAddress"
EMAIL_SUBJECT_PREFIX = SITE_NAME + " "

# Paramètres cartographiques et géographiques
# Fonds de carte par défaut
CUSTOM_TILES = [
    (
        "OpenStreetMap Mapnik",
        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    ),
    (
        "OpenTopoMap",
        "https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png",
        '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>',
    ),
]
# Projection par défaut des tables de données cartographiques du projet
GEO_PROJECTION = geoDefaultProj
# Latitude et longitude du centroid du territoire couvert par dbChiro[web]
MAP_Y = geoDefaultY
MAP_X = geoDefaultX
# Niveau de zoom par défaut des cartes LeafLet (pour afficher tout le territoire)
MAP_DEFAULT_ZOOM = 8
# Clé permettant d'accéder aux couches de l'IGN via leur API
# A commander ici (une version de base est gratuite pour un usage grand public limité): http://professionnels.ign.fr/ign/contrats
IGN_API_KEY = "yourapikey"
# Clé permettant d'accéder à des couches MapBox
# https://www.mapbox.com/maps/
MAPBOX_API_KEY = "yourapikey"
# Altitude mini et maxi possible à la saisie des localités.
ALTITUDE_MAX = 4800
ALTITUDE_MIN = -20


#  Cycle annuel des chiroptères pour l'attribution auto de la période à une observation

# doy                   | 335	    | 60	    | 136	    | 228
# date                  | 01/12     | 01/03	    | 16/05	    | 16/08
# ----------------------+-----------+-----------+-----------+----------
# wintering (w)         |     ≥	    |     <     |           |
# sping transit (st)    |   	    |     ≥	    |     <     |
# Summering (e)		    |	        |           |     ≥	    |    <
# Automn transit (ta)	|     <     |           |     ≥     |

PERIOD_WINTERING_START = 335
PERIOD_WINTERING_VALUE = "Hivernant"
PERIOD_SPRING_TRANSIT_START = 60
PERIOD_SPRING_TRANSIT_VALUE = "Transit printanier"
PERIOD_START_SUMMERING_START = 136
PERIOD_START_SUMMERING_VALUE = "Estivage"
PERIOD_START_AUTUMN_TRANSIT_START = 228
PERIOD_START_AUTUMN_TRANSIT_VALUE = "Transit automnal"
