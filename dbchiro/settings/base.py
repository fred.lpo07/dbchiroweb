"""
Django settings for dbchiro project.

Generated by 'django-admin startproject' using Django 1.11.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""
import logging
from os.path import abspath, dirname, join

from .configuration import config

logger = logging.getLogger(__name__)


def root(*dirs):
    base_dir = join(dirname(__file__), "../..")
    return abspath(join(base_dir, *dirs))


BASE_DIR = root()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!

# setting_file = 'settings/settings.json'


# with open(root(setting_file)) as f:
# params = json.loads(f.read())

#
# def get_setting(setting, params=params):
#     try:
#         return params[setting]
#     except KeyError:
#         error_msg = 'Set the {0} environnement variable'.format(setting)
#         raise ImproperlyConfigured(error_msg)


SECRET_KEY = config.SECRET_KEY

SITE_NAME = config.SITE_NAME

BASE_HOSTS = ["localhost", "127.0.0.1", "[::1]"]

if config.CUSTOM_HOSTS:
    ALLOWED_HOSTS = BASE_HOSTS + config.CUSTOM_HOSTS
else:
    ALLOWED_HOSTS = BASE_HOSTS
# Application definition

MAIN_APPS = [
    "dal",
    "dal_select2",
    "nested_admin",
    "registration",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.gis",
    "django.contrib.admindocs",
    "ckeditor",
    "leaflet",
    "crispy_forms",
    "djgeojson",
    "django_tables2",
    "django_filters",
    "django_extensions",
    "rest_framework",
    "rest_framework_gis",
    "drf_yasg",
]

PROJECT_APPS = [
    "core",
    "accounts",
    "geodata",
    # "metadata",
    "management",
    "dicts",
    "sights",
    "blog",
    "api",
    "synthesis",
]

# INSTALLED_APPS = PREREQ_APPS + PROJECT_APPS

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

ROOT_URLCONF = "dbchiro.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            root("templates"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "dbchiro.settings.helper.context_processors.site_info",
            ],
        },
    },
]

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
    }
}

# CACHES = {
#     "default": {
#         "BACKEND": "django_redis.cache.RedisCache",
#         "LOCATION": "redis://127.0.0.1:6379/1",
#         "OPTIONS": {
#             "CLIENT_CLASS": "django_redis.client.DefaultClient",
#         },
#     }
# }
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"
CACHE_TTL = 60 * 15


WSGI_APPLICATION = "dbchiro.wsgi.application"

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": config.DB_NAME,  # Indiquez ici le nom de la base de données
        "USER": config.DB_USER,
        "PASSWORD": config.DB_PWD,  # Changer password par le mot de passe du superutilisateur
        "HOST": config.DB_HOST,
        "PORT": config.DB_PORT,  # Changer password par le mot de passe du superutilisateur
    }
}

# Customizing User model

if hasattr(config, "ADMINS"):
    ADMINS = config.ADMINS
else:
    ADMINS = ((config.EMAIL_USER, config.EMAIL_USER),)

AUTH_USER_MODEL = "accounts.Profile"

LOGIN_REDIRECT_URL = "blog:home"
LOGOUT_REDIRECT_URL = "auth_login"

try:
    if type(config.REGISTRATION_APPROVAL) == bool:
        REGISTRATION_APPROVAL = config.REGISTRATION_APPROVAL
except AttributeError:
    REGISTRATION_APPROVAL = True

REGISTRATION_FORM = "accounts.forms.UserRegisterForm"

# Registration
# TODO: set desired activation window in days
ACCOUNT_ACTIVATION_DAYS = (
    7  # One-week activation window; you may, of course, use a different value.
)
REGISTRATION_AUTO_LOGIN = True  # Automatically log the user in.
if hasattr(config, "REGISTRATION_ADMINS"):
    REGISTRATION_ADMINS = config.REGISTRATION_ADMINS
else:
    REGISTRATION_ADMINS = ADMINS

# Email parameters
EMAIL_HOST = config.EMAIL_SMTP
EMAIL_USE_SSL = config.EMAIL_SSL
EMAIL_PORT = config.EMAIL_PORT
EMAIL_HOST_USER = config.EMAIL_USER
EMAIL_HOST_PASSWORD = config.EMAIL_PWD
EMAIL_SUBJECT_PREFIX = config.EMAIL_SUBJECT_PREFIX
DEFAULT_FROM_EMAIL = config.EMAIL_DEFAULT_FROM

# RestFramework settings
REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "PAGE_SIZE": 1000,
    # "DEFAULT_RENDERER_CLASSES": ("rest_framework.renderers.JSONRenderer",),
}

# Database SRID (2154 for Lambert93, 4326 for WGS84, etc.)
# TODO: set database SRID for geographic models
# More information here :
# English : https://en.wikipedia.org/wiki/Spatial_reference_system#Identifier
# French : https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_coordonn%C3%A9es_(cartographie)
GEODATA_SRID = config.GEO_PROJECTION
GEODATA_ALTI_MAX = config.ALTITUDE_MAX
GEODATA_ALTI_MIN = config.ALTITUDE_MIN

#  Annual cycle bat periods

# doy                   | 335	    | 60	    | 136	    | 228
# date                  | 01/12     | 01/03	    | 16/05	    | 16/08
# ----------------------+-----------+-----------+-----------+----------
# wintering (w)         | ≥	        | <         |           |
# sping transit (st)    |   	    |  ≥	    | <         |
# Summering (e)		    |	        |           | ≥	        | <
# Automn transit (ta)	| <			|           | ≥         |

PERIOD_WINTERING_START = config.PERIOD_WINTERING_START
PERIOD_WINTERING_VALUE = config.PERIOD_WINTERING_VALUE
PERIOD_SPRING_TRANSIT_START = config.PERIOD_SPRING_TRANSIT_START
PERIOD_SPRING_TRANSIT_VALUE = config.PERIOD_SPRING_TRANSIT_VALUE
PERIOD_AUTUMN_TRANSIT_START = config.PERIOD_START_AUTUMN_TRANSIT_START
PERIOD_AUTUMN_TRANSIT_VALUE = config.PERIOD_START_AUTUMN_TRANSIT_VALUE
PERIOD_SUMMERING_START = config.PERIOD_START_SUMMERING_START
PERIOD_SUMMERING_VALUE = config.PERIOD_START_SUMMERING_VALUE

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = "fr-fr"

TIME_ZONE = "Europe/Paris"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_ROOT = root("static")
STATIC_URL = "/static/"

MEDIA_ROOT = root("media")
MEDIA_URL = "/media/"

DEFAULT_FILE_STORAGE = "django.core.files.storage.FileSystemStorage"

# CrispyForms Settings

CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap3"

CRISPY_TEMPLATE_PACK = "bootstrap3"

CRISPY_CLASS_CONVERTER = {
    "inputelement": None,
    "errorcondition": "is-invalid-input",
}

# DjangoLeaflet Settings

ignAPIkey = config.IGN_API_KEY
mapboxAPIkey = config.MAPBOX_API_KEY

# MapLayers
TILES = []

MAPBOX_TILES = [
    # Fond aérien, nécessite un compte MapBox pour obtenir la clé.
    (
        "Vue satellite (MapBox)",
        "//api.mapbox.com/styles/v1/mapbox/satellite-streets-v9/tiles/256/{z}/{x}/{y}?access_token="
        + mapboxAPIkey,
        "(c) MapBox",
    ),
]

IGN_TILES = [  # Fond SCAN IGN, nécessite un compte IGN pour obtenir la clé.
    (
        "Scan25 IGN",
        "//gpp3-wxs.ign.fr/"
        + ignAPIkey
        + "/geoportail/wmts?LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&EXCEPTIONS=text/xml&FORMAT=image/jpeg&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&STYLE=normal&TILEMATRIXSET=PM&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
        "(c) IGN Geoportail",
    ),
    (
        "Orthophoto IGN",
        "//gpp3-wxs.ign.fr/"
        + ignAPIkey
        + "/geoportail/wmts?LAYER=ORTHOIMAGERY.ORTHOPHOTOS&EXCEPTIONS=text/xml&FORMAT=image/jpeg&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&STYLE=normal&TILEMATRIXSET=PM&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
        "(c) IGN Geoportail",
    ),
]

try:
    if config.CUSTOM_TILES:
        TILES = TILES + config.CUSTOM_TILES
except AttributeError:
    pass

if ignAPIkey != "yourapikey":
    TILES = TILES + IGN_TILES

if mapboxAPIkey != "yourapikey":
    TILES = TILES + MAPBOX_TILES

LEAFLET_CONFIG = {
    "DEFAULT_CENTER": (config.MAP_Y, config.MAP_X),
    "DEFAULT_ZOOM": config.MAP_DEFAULT_ZOOM,
    "MIN_ZOOM": 3,
    "MAX_ZOOM": 18,
    "MINIMAP": True,
    "TILES": TILES,
    "SCALE": "both",
    "PLUGINS": {
        "forms": {"auto-include": True},
        "spin": {
            "js": [
                "/static/js/leaflet/spin.min.js",
                "/static/js/leaflet/leaflet.spin.min.js",
            ],
        },
        "locate": {
            "js": "/static/js/leaflet/L.Control.Locate.min.js",
            "css": "/static/css/L.Control.Locate.css",
        },
        "search": {
            "js": "/static/js/leaflet/leaflet-search.min.js",
            "css": "/static/css/leaflet-search.css",
        },
        "cluster": {
            "js": "/static/js/leaflet/leaflet.markercluster.js",
            "css": [
                "/static/css/MarkerCluster.css",
                "/static/css/MarkerCluster.Default.css",
            ],
        },
        "fullscreen": {
            "js": "/static/js/leaflet/Leaflet.fullscreen.min.js",
            "css": "/static/css/leaflet.fullscreen.css",
        },
        "ajax": {
            "js": "/static/js/leaflet/leaflet.ajax.min.js",
        },
    },
}

# CkEditor Settings

AUTHENTICATION_BACKENDS = ("django.contrib.auth.backends.ModelBackend",)  # default

INTERNAL_IPS = "127.0.0.1"

IMAGE_FILE_TYPES = ["png", "jpg", "jpeg"]
DOCUMENT_FILE_TYPES = ["doc", "docx", "odt", "pdf", "xls", "csv", "xlsx", "ods"]


# Read-only access for all non-sensitive datas

try:
    SEE_ALL_NON_SENSITIVE_DATA = config.SEE_ALL_NON_SENSITIVE_DATA
except AttributeError:
    SEE_ALL_NON_SENSITIVE_DATA = False
