import logging

import coloredlogs

from dbchiro.settings.base import *

logger = logging.getLogger(__name__)

coloredlogs.install(level="DEBUG")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DEV_APPS = ["debug_toolbar"]

INSTALLED_APPS = MAIN_APPS + DEV_APPS + PROJECT_APPS

INTERNAL_IPS = [
    "127.0.0.1",
]

DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": lambda request: True,
}
# LOGGING_CONFIG = None
format = "%(asctime)s %(name)s[%(process)d] %(levelname)s %(message)s"


LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "coloredlogs": {
            "()": "coloredlogs.ColoredFormatter",
            "fmt": "[%(asctime)s] %(name)s %(levelname)s %(message)s",
        },
    },
    "handlers": {
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": "/tmp/dbchiro-debug.log",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "coloredlogs",
        },
    },
    "loggers": {
        "": {
            "handlers": ["file", "console"],
            "level": "WARNING",
            "propagate": True,
        },
        "django.db.backends": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
        },
        "django": {
            "handlers": ["file", "console"],
            "level": "WARNING",
            "propagate": True,
        },
        "django.requests": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "accounts": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "api": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "blog": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "core": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "dbchiro": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "dicts": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "geodata": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "metadata": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "management": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "sights": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
        "synthesis": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
    },
}

if DEBUG:
    # make all loggers use the console.
    for logger in LOGGING["loggers"]:
        LOGGING["loggers"][logger]["handlers"] = ["console"]
