.. _`docker-install`:

************************
Installation sous Docker
************************

**Prerequis**:

* Un serveur linux 64 bits avec un OS récent (testé sous ubuntu >= 18.04, debian >= 9 )
* Un émulateur de terminal pour la connexion via ssh (natif sous Linux et osX, `Putty <http://www.chiark.greenend.org.uk/~sgtatham/putty/>`_ ou `mobaXterm <https://mobaxterm.mobatek.net/>`_ sous windows)

Installation de Docker
======================

Installation de Docker community edition
----------------------------------------

* Pour Debian: https://docs.docker.com/install/linux/debian 
* Pour Ubuntu: https://docs.docker.com/install/linux/ubuntu


Installation de docker-compose
------------------------------

.. code-block:: sh

    apt install python3 python3-pip
    python3 -m pip install docker-compose

Création du conteneur dbchiro
=============================

Dans un dossier créé pour l'occasion, crééz un fichier ``docker-compose.yaml`` avec le contenu suivant:

.. code-block:: yaml

    version: "3"                                                         

    services:

      db: 
        image: mdillon/postgis:11-alpine
        container_name: dbchiro_db
        volumes:
          - $POSTGRES_DATA_DIR:/var/lib/postgresql/data
        environment:
          POSTGRES_DB: $POSTGRES_DB
          POSTGRES_USER: $POSTGRES_USER
          POSTGRES_PASSWORD: $POSTGRES_PASSWORD


      dbchiro:
        image: dbchiro/dbchiroweb:alpine
        container_name: dbchiro_app
        volumes:
          - $DBCHIRO_APP_DIR:/dbchiro
        links:
          - db
        ports:
          - $DBCHIRO_PORT:80
        environment:
          POSTGRES_DB: $POSTGRES_DB
          POSTGRES_USER: $POSTGRES_USER
          POSTGRES_PASSWORD: $POSTGRES_PASSWORD
          SRID: $SRID
          SETTINGS: $SETTINGS
          SU_USERNAME: $SU_USERNAME
          SU_PWD: $SU_PWD
          SU_EMAIL: $SU_EMAIL
          IGN_API_KEY: $IGN_API_KEY

Dans ce même dossier, créez un fichier ``.env`` contenant les informations suivantes:

.. code-block:: ini 

    #Postgis db
    POSTGRES_DB=dbchirodb
    POSTGRES_USER=dbuser
    POSTGRES_PASSWORD=dbpwd123
    POSTGRES_DATA_DIR=local_path_to_pg_data_dir

    #dbChiro App
    DBCHIRO_APP_DIR=local_path_to_app_dir
    DBCHIRO_PORT=80
    SRID=4326
    SETTINGS=production
    SU_USERNAME=myusername
    SU_PWD=myuserpwd
    SU_EMAIL=mysuername@mydomain.tld
    IGN_API_KEY=pratique


Dans ce dossier, lancez le conteneur avec la commande ``docker-compose up -d``.

La première installation peut-être longue avant que le service ne soit accessible, car il nécessite le chargement des données initiales (dictionnaires et surtout communes de France).

Vous pouvez en suivre l'avancement avec la commande ``docker logs -f dbchiro_app``.

