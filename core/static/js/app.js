/*****************************************************************
 * Fix Bootstrap droprown-menu hidden in table bottom in panels  *
 *****************************************************************/

// fix menu overflow under the responsive table
// Source: https://stackoverflow.com/questions/26018756/bootstrap-button-drop-down-inside-responsive-table-not-visible-because-of-scroll
$(document).click(function (event) {
    //hide all our dropdowns
    $('.dropdown-menu[data-parent]').hide()
})

$(document).on('click', '.table-responsive [data-toggle="dropdown"]', () => {
    // if the button is inside a modal
    if ($('body').hasClass('modal-open')) {
        throw new Error(
            'This solution is not working inside a responsive table inside a modal, you need to find out a way to calculate the modal Z-index and add it to the element'
        )
    }

    $buttonGroup = $(this).parent()
    if (!$buttonGroup.attr('data-attachedUl')) {
        const ts = +new Date()
        $ul = $(this).siblings('ul')
        $ul.attr('data-parent', ts)
        $buttonGroup.attr('data-attachedUl', ts)
        $(window).resize(function () {
            $ul.css('display', 'none').data('top')
        })
    } else {
        $ul = $('[data-parent=' + $buttonGroup.attr('data-attachedUl') + ']')
    }
    if (!$buttonGroup.hasClass('open')) {
        $ul.css('display', 'none')
        return
    }
    dropDownFixPosition($(this).parent(), $ul)

    const dropDownFixPosition = (button, dropdown) => {
        const dropDownTop = button.offset().top + button.outerHeight()
        dropdown.css('top', dropDownTop + 'px')
        dropdown.css('left', button.offset().left + 'px')
        dropdown.css('position', 'absolute')
        dropdown.css('width', dropdown.width())
        dropdown.css('heigt', dropdown.height())
        dropdown.css('display', 'block')
        dropdown.appendTo('body')
    }
})

/********************
 *    SIDEBAR       *
 ********************/
$(document).ready(function () {
    const trigger = $('.hamburger'),
        overlay = $('.overlay')
    let isClosed = false

    trigger.click(() => {
        hamburger_cross()
    })

    const hamburger_cross = () => {
        if (isClosed == true) {
            overlay.hide()
            trigger.removeClass('is-open')
            trigger.addClass('is-closed')
        } else {
            overlay.show()
            trigger.removeClass('is-closed')
            trigger.addClass('is-open')
        }
        isClosed = !isClosed
    }

    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled')
    })
})

/****************
 * Map objects  *
 * **************/

const legendElement = (title, color, radius) => {
    const span = document.createElement('span')
    icon = document.createElement('i')
    icon.style.background = color
    icon.style.borderRadius = radius ? radius : null
    span.textContent = title
    span.prepend(icon)
    span.append(document.createElement('br'))
    return span
}

const showDisclaimer = () => {
    const div = document.getElementById('info legend')
    while (div.hasChildNodes()) {
        div.removeChild(div.lastChild)
    }
    const elements = [
        legendElement('Localisation simple', '#ff4c41'),
        legendElement('Localisation sur un métasite', '#8A2BE2'),
        legendElement('Emprise du métasite', '#9370DB', 0),
    ]
    div.append(...elements)
    return
}

const hideDisclaimer = () => {
    const div = document.getElementById('info legend')
    while (div.hasChildNodes()) {
        div.removeChild(div.lastChild)
    }
    const icon = document.createElement('i')
    icon.classList.add('fa', 'fa-info-circle')
    div.append(icon)
    return
}

const actionButtons = (label, style, icon, url) => {
    const a = document.createElement('a')
    const i = document.createElement('i')
    a.classList.add(`text-${style}`)
    i.classList.add('fa', 'fa-fw', icon)
    a.href = url
    a.target = '_blank'
    a.append(i)
    // a.type = 'button';
    a.title = label
    return a
}

const placeActionButtonGroup = (id, model, id_session = null) => {
    const btnGroup = document.createElement('div')
    // btnGroup.classList.add('btn-group', 'btn-group-xs');
    btnGroup.append(
        actionButtons(
            'Fiche détaillée',
            'primary',
            'fa-info-circle',
            `/${model}/${id}/detail`
        ),
        actionButtons('Modifier', 'info', 'fa-edit', `/${model}/${id}/update`),
        actionButtons(
            'Ajouter une observation détaillée',
            'success',
            'fa-calendar-alt',
            `/${model}/${id}/session/add`
        ),
        // actionButtons(
        //     'Supprimer',
        //     'danger',
        //     'fa-trash',
        //     `/${model}/${id}/delete`
        // )
    )
    return btnGroup.outerHTML
}

const sightingActionButtonGroup = (id, model, id_session = null) => {
    const btnGroup = document.createElement('div')
    // btnGroup.classList.add('btn-group', 'btn-group-xs');
    btnGroup.append(
        actionButtons(
            'Fiche détaillée',
            'primary',
            'fa-info-circle',
            `/${model}/${id}/detail`
        ),
        actionButtons(
            'Modifier',
            'info',
            'fa-edit',
            `/session/${id_session}/sighting/edit`
        )
    )
    return btnGroup.outerHTML
}

/*******************
 * Place search    *
 ********************/

const placeSearchBlock = (dataUrl, userId, onlyMyPlaces) => {
    console.debug(dataUrl, onlyMyPlaces,userId)
    let mutableDataUrl = `${dataUrl}?`;
    const switchOnlyMine = () => {
        console.log('Test Check',$('#only_mine').prop('checked'))
        if ($('#only_mine').prop('checked')) {
            console.debug('OnlyMine checked')
            $('#created_by_id').val(userId)
        } else {
            console.debug('OnlyMine unchecked')
            $('#created_by_id').val('')
        }
    }

    // $('#only_mine').change(switchOnlyMine);

    if (onlyMyPlaces) {
        $('#only_mine').prop('checked', true)
        // $('#created_by_id').val(userId)
    };

    const moreInfoElement = (label, value) => {
        const p = document.createElement('li')
        const strong = document.createElement('strong')
        strong.textContent = `${label} : `
        p.textContent = value
        p.prepend(strong)
        return value ? p : null
    }

    const format = (d) => {
        const {
            last_session: last_session,
            contact,
            comment,
            is_managed,
            convention,
            creator,
            timestamp_create,
        } = d.properties
        const div = document.createElement('ul')
        const dataToDisplay = [
            // moreInfoElement('Dernière session', last_session?.date_start__max),
            // moreInfoElement("Types d'observations", contact?.contacts),
            moreInfoElement('Commentaire', comment),
            moreInfoElement('Site géré', is_managed ? 'Oui' : 'Non'),
            moreInfoElement('Convention refuge', convention ? 'Oui' : 'Non'),
            moreInfoElement(
                'Création',
                creator != null
                    ? `le ${new Date(
                          timestamp_create
                      ).toLocaleDateString()} par ${creator.get_full_name}`
                    : new Date(timestamp_create).toLocaleDateString()
            ),
            //     data: (row) => {
            //         const creationDate = new Date(
            //             row.properties.timestamp_create
            //         ).toLocaleDateString()
            //         const creationContent =
            //             row.properties.creator != null
            //                 ? `${creationDate} par ${row.properties.creator.get_full_name}`
            //                 : creationDate
            //         return creationContent
            //     },
            // },
        ]
        div.append(...dataToDisplay.filter((p) => p !== null))
        return div
    }

    const table = $('#place_results').DataTable({
        language: dataTableLanguage,
        dom: 'lrtip',
        paging: false,
        scrollY: '500px',
        searching: false,
        columns: [
            {
                class: 'details-control',
                orderable: false,
                data: '',
                defaultContent:
                    '<a><i class="fa fa-fw fa-plus-circle text-success"></i></a>',
            },
            {
                data: (row) => {
                    return placeActionButtonGroup(row.id, 'place')
                },
                width: '100',
            },
            {
                data: (row) =>
                    `<a href="/place/${row.id}/detail" target="_blank">${row.properties.name}</a>`,
            },
            { data: 'properties.altitude' },
            {
                data: (row) => {
                    const data = row.properties.municipality_data
                    return data?.name ? `${data.name} (${data.code})` : null
                },
            },
            {
                data: function (row) {
                    const data = row.properties.metaplace_data
                    return data?.name
                        ? `<a href="/metaplace/${data.id_metaplace}/detail" target="_blank">${data.name}</a>`
                        : null
                },
            },
            {
                data: (row) => {
                    return row.properties.type_data?.descr != null
                        ? row.properties.type_data.descr
                        : null
                },
            },
        ],
    })

    const detailRows = []

    $('#place_results tbody').on('click', 'tr td.details-control', function () {
        const tr = $(this).closest('tr')
        const row = table.row(tr)
        const idx = $.inArray(tr.attr('id'), detailRows)

        if (row.child.isShown()) {
            tr.removeClass('details')
            row.child.hide()

            // Remove from the 'open' array
            detailRows.splice(idx, 1)
        } else {
            tr.addClass('details')
            row.child(format(row.data())).show()

            // Add to the 'open' array
            if (idx === -1) {
                detailRows.push(tr.attr('id'))
            }
        }
    })

    // On each draw, loop over the `detailRows` array and show any child rows
    table.on('draw', function () {
        $.each(detailRows, function (i, id) {
            $('#' + id + ' td.details-control').trigger('click')
        })
    })

    // {#var dataurl = '{% url "api:geodata_place" %}';#}
    // MAP Scripts

    window.addEventListener('map:init', function (event) {
        addSpinner()
        $('#spinner').hide()
        const searchMap = event.detail.map

        const metaplace_dataurl = '/metaplace/api/search?format=json'
        let circleMarker = null

        const markers = L.markerClusterGroup({
            chunkedLoading: true,
            spiderfyDistanceMultiplier: 3,
            maxClusterRadius: 14,
        })

        const style_data = {
            radius: 6,
            fillColor: '#ff4c41',
            color: '#ddd',
            weight: 2,
            opacity: 1,
            fillOpacity: 0.8,
        }

        const style_data_metasite = {
            radius: 6,
            fillColor: '#8A2BE2',
            color: '#ddd',
            weight: 2,
            opacity: 1,
            fillOpacity: 0.8,
        }

        const polygon_style = {
            color: '#8A2BE2',
            fillColor: '#9370DB',
            weight: 2,
            opacity: 0.8,
            fillOpacity: 0.2,
        }

        // Fonction plein écran
        searchMap.addControl(new L.Control.Fullscreen())

        // Géolocalisation de l'utilisateur

        const locateOptions = {
            flyTo: true,
            title: 'Ma position',
            icon: 'fa fa-location-arrow',
        }

        L.control.locate(locateOptions).addTo(searchMap)

        // Recherche par adresse
        searchMap.addControl(new L.Control.Search(searchControlOptions))

        // Add legend
        const legend = L.control({ position: 'bottomleft' })

        legend.onAdd = function (map) {
            let div = L.DomUtil.create('div', 'info legend')
            div.setAttribute('onmouseenter', 'showDisclaimer()')
            div.setAttribute('onmouseleave', 'hideDisclaimer()')
            div.id = 'info legend'
            div.innerHTML = "<i class='fa fa-info-circle'></i>"
            return div
        }

        legend.addTo(searchMap)

        // Download GeoJSON data with Ajax
        let cancelMetaPlaceSearchRequest
        loadMetaPlace = function () {
            if (cancelMetaPlaceSearchRequest) {
                // Cancel the previous request before making a new request
                cancelMetaPlaceSearchRequest.cancel()
            }
            // Create a new CancelToken
            cancelMetaPlaceSearchRequest = axios.CancelToken.source()
            dataLoaded.metaPlace = false
            displaySpinner(checkLoadStates(dataLoaded))
            $http
                .get(metaplace_dataurl, {
                    progress: true,
                    cancelToken: cancelMetaPlaceSearchRequest.token,
                })
                .then(function (resp) {
                    if (resp.status == 200) {
                        return resp.data
                    }
                })
                .catch(function (error) {
                    // handle error
                    console.log('<loadMetaPlace Axios error>', error)
                })
                .then(function (data) {
                    L.geoJSON(data, {
                        style: polygon_style,
                        onEachFeature: function onEachFeature(feature, layer) {
                            const props = feature.properties
                            const content =
                                '<h3>MetaSite&nbsp;:&nbsp;' +
                                props.name +
                                '</h3><p><ul><li><b>Type</b>: ' +
                                (props.type_data
                                    ? props.type_data.descr
                                    : 'Non défini') +
                                '</li></ul> </p><div class="text-right"><div class="btn-group" role="group"><a type="button" class="btn btn-default btn-sm" align="center" style="color:white;" href="/metaplace/' +
                                feature.id +
                                '/detail" target="_blank"><i class="fa fa-fw fa-info-circle"></i></a><a type="button" class="btn btn-info btn-sm" align="center" style="color:white;" href="/metaplace/' +
                                feature.id +
                                '/update" target="_blank"><i class="fa fa-fw fa-edit"></i></a></div></div>'
                            layer.bindPopup(content)
                        },
                    })
                        .addTo(searchMap)
                        .bringToBack()
                })
                .then(function () {
                    dataLoaded.metaPlace = true
                    displaySpinner(checkLoadStates(dataLoaded))
                })
                .catch(function (error) {
                    console.log('<loadMetaPlace Leaflet error>', error)
                })
        }

        let cancelPlaceSearchRequest
        // Download GeoJSON data with Ajax
        let markersById = {}
        const loadPlaces = function () {
            table.clear().draw()
            markers.clearLayers()
            dataLoaded.Place = false
            displaySpinner(checkLoadStates(dataLoaded))
            if (cancelPlaceSearchRequest) {
                // Cancel the previous request before making a new request
                cancelPlaceSearchRequest.cancel()
            }
            // Create a new CancelToken
            cancelPlaceSearchRequest = axios.CancelToken.source()
            // Create a new CancelToken

            $http
                .get(mutableDataUrl, {
                    progress: true,
                    cancelToken: cancelPlaceSearchRequest.token,
                })
                .then(function (resp) {
                    if (resp.status == 200) {
                        return resp.data
                    }
                })
                .catch(function (error) {
                    // handle error
                    console.log('<loadPlaces Error1>', error)
                })
                .then(function (data) {
                    const locationLayer = L.geoJson(data, {
                        onEachFeature: function onEachFeature(feature, layer) {
                            const props = feature.properties
                            const content = `
                            <p>
                            <h4>${props.name}</h4>
                            <span class="label label-${props.type_data ? 'success':'default'}">${
                                props.type_data
                                    ? props.type_data.code
                                    : 'nd'
                            }</span> ${props.type_data
                            ? props.type_data.descr
                            : 'Type non défini'}
                            </p>
                            <p>
                            <span class="pull-right">
                                ${placeActionButtonGroup(feature.id, 'place')}
                                </span>
                            </p>

                            `
                            layer.bindPopup(content)
                            if (feature.id) {
                                markersById[feature.id] = layer
                            }
                        },
                        pointToLayer: function (feature, latlng) {
                            // Set a different color if location is in a metaplace
                            if (feature.properties.metaplace_data) {
                                return L.circleMarker(
                                    latlng,
                                    style_data_metasite
                                )
                            }
                            return L.circleMarker(latlng, style_data)
                        },
                    })
                    markers
                        .addLayer(locationLayer)
                        .addTo(searchMap)
                        .bringToFront()
                    searchMap.fitBounds(locationLayer.getBounds())
                    searchMap.invalidateSize()
                    table.rows.add(data.features).draw()
                })
                .then(
                    function () {
                        dataLoaded.Place = true
                        displaySpinner(checkLoadStates(dataLoaded))
                    },
                    function (e) {
                        // ceci n'est pas appelé
                        console.log('rompue', e)
                    }
                )
                .catch(function (error) {
                    console.log('<loadPlaces Error2>', error)
                })
        }

        $('#place_results').on('click', 'tbody td', function () {
            const markerId = table.row(this).data().id
            const markerPosition = markersById[markerId].getLatLng()
            let circleMarker

            if (circleMarker != null) {
                searchMap.removeLayer(circleMarker)
            }
            circleMarker = new L.circleMarker(markerPosition, {
                radius: 20,
                color: 'red',
                fillColor: 'transparent',
            })
                .addTo(searchMap)
                .bringToBack()
            searchMap.setView(markerPosition, 15, {
                animation: true,
            })
        })

        const loadDatas = () => {
            loadPlaces()
            loadMetaPlace()
        }

        const clearLayers = () => {
            metaPlacePolygons.clearLayers()
            placeMarkers.clearLayers()
        }

        const showValues = () => {
            const str = $('#searchForm input, select')
                .map(function () {
                    return $(this).val().trim() == '' ? null : this
                })
                .serialize()
            $('#results').text(str)
            mutableDataUrl = `${dataUrl}?${str}`
            loadDatas()
        }

        let debounce = null
        $('#searchForm').on('click change', 'input, select', function () {
            clearTimeout(debounce)
            debounce = setTimeout(showValues, 500)
        })

        showValues()
    })
}

const placeDetailMap = () => {
    const map_init_basic = (map, options, marker) => {
        // Géolocalisation de l'utilisateur

        const locateOptions = {
            flyTo: true,
            title: 'Ma position',
            icon: 'fa fa-location-arrow',
        }

        L.control.locate(locateOptions).addTo(map)

        // Fonction plein écran
        map.addControl(new L.Control.Fullscreen())

        // Ajout du marker de la localité
        L.geoJson(marker).addTo(map)
        map.fitBounds(L.geoJson(marker).getBounds())
        map.setZoom(13)
    }
}

// Sighting Search

const sightingSearchBlock = () => {
    const aLink = (url, label) => {
        const a = document.createElement('a')
        a.href = url
        a.target = '_blank'
        a.title = `Voir la page de détails de ${label}`
        a.innerHTML = label
        return a.outerHTML
    }
    const format = (d) => {
        return (
            '<strong>Commentaire</strong>&nbsp;:&nbsp;' +
            (d.properties.comment ? d.properties.comment : 'Aucun commentaire')
        )
    }

    const table = $('#sight_results').DataTable({
        language: dataTableLanguage,
        paging: false,
        scrollY: '500px',
        searching: false,
        columns: [
            {
                class: 'details-control',
                orderable: false,
                data: '',
                defaultContent:
                    '<i class="fa fa-fw fa-plus-circle text-success"></i></a>',
            },
            {
                data: (row) => {
                    return sightingActionButtonGroup(
                        row.id,
                        'sighting',
                        row.properties.session_data.id_session
                    )
                },
                width: '100',
            },
            {
                data: function (row) {
                    var specie = row.properties.specie_data.sci_name
                    if (row.properties.specie_data.sp_true == true) {
                        specie = specie + '&nbsp;(Espèce vraie)'
                    }
                    return specie
                },
            },

            { data: 'properties.total_count' },
            {
                data: function (row) {
                    var breed_colo = null
                    if (row.properties.breed_colo == true) {
                        breed_colo = 'Oui'
                    }
                    return breed_colo
                },
            },
            {
                data: (row) => {
                    return aLink(
                        `/place/${row.properties.session_data.place_data.id_place}/detail`,
                        row.properties.session_data.place_data.name
                    )
                },
            },
            {
                data: function (row) {
                    if (
                        row.properties.session_data.place_data.municipality_data
                    ) {
                        if (
                            row.properties.session_data.place_data
                                .municipality_data.name &&
                            row.properties.session_data.place_data
                                .municipality_data.code
                        ) {
                            return (
                                row.properties.session_data.place_data
                                    .municipality_data.name +
                                ' (' +
                                row.properties.session_data.place_data
                                    .municipality_data.code +
                                ')'
                            )
                        }
                    } else {
                        return null
                    }
                },
            },
            { data: 'properties.session_data.date_start' },
            {
                data: (row) => {
                    return aLink(
                        `/session/${row.properties.session_data.id_session}/detail`,
                        row.properties.session_data.name
                    )
                },
            },
            { data: 'properties.session_data.contact_data.descr' },
            {
                data: (row) =>
                    row.properties.creator
                        ? row.properties.creator.get_full_name
                        : 'inconnu',
            },
        ],
    })

    var detailRows = []

    $('#sight_results tbody').on('click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr')
        var row = table.row(tr)
        var idx = $.inArray(tr.attr('id'), detailRows)

        if (row.child.isShown()) {
            tr.removeClass('details')
            row.child.hide()

            // Remove from the 'open' array
            detailRows.splice(idx, 1)
        } else {
            tr.addClass('details')
            row.child(format(row.data())).show()

            // Add to the 'open' array
            if (idx === -1) {
                detailRows.push(tr.attr('id'))
            }
        }
    })

    // On each draw, loop over the `detailRows` array and show any child rows
    table.on('draw', function () {
        $.each(detailRows, function (i, id) {
            $('#' + id + ' td.details-control').trigger('click')
        })
    })

    // MAP Scripts
    window.addEventListener('map:init', function (event) {
        addSpinner()
        $('#spinner').hide()
        var searchMap = event.detail.map

        var circleMarker = null

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution:
                '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        }).addTo(searchMap)

        var markers = L.markerClusterGroup({
            chunkedLoading: true,
            spiderfyDistanceMultiplier: 3,
            maxClusterRadius: 14,
        })

        var style_data = {
            radius: 6,
            fillColor: '#ff4c41',
            color: '#ddd',
            weight: 2,
            opacity: 1,
            fillOpacity: 0.8,
        }

        var style_data_metasite = {
            radius: 6,
            fillColor: '#8A2BE2',
            color: '#ddd',
            weight: 2,
            opacity: 1,
            fillOpacity: 0.8,
        }

        var polygon_style = {
            color: '#8A2BE2',
            fillColor: '#9370DB',
            weight: 2,
            opacity: 0.8,
            fillOpacity: 0.2,
        }

        // Fonction plein écran
        searchMap.addControl(new L.Control.Fullscreen())

        // Géolocalisation de l'utilisateur

        var locateOptions = {
            flyTo: true,
            title: 'Ma position',
            icon: 'fa fa-location-arrow',
        }

        L.control.locate(locateOptions).addTo(searchMap)

        // Recherche par adresse
        searchMap.addControl(new L.Control.Search(searchControlOptions))

        // Add legend
        let legend = L.control({ position: 'bottomleft' })

        legend.onAdd = function (map) {
            let div = L.DomUtil.create('div', 'info legend')
            div.setAttribute('onmouseenter', 'showDisclaimer()')
            div.setAttribute('onmouseleave', 'hideDisclaimer()')
            div.id = 'info legend'
            div.innerHTML = "<b class='fa fa-info-circle'></b>"
            return div
        }

        legend.addTo(searchMap)

        // Download GeoJSON data with Ajax
        let markersById = {}
        let cancelSightingSearchRequest
        const loadSightings = function () {
            table.clear().draw()
            markers.clearLayers()
            displaySpinner(true)
            if (cancelSightingSearchRequest) {
                // Cancel the previous request before making a new request
                cancelSightingSearchRequest.cancel()
            }
            // Create a new CancelToken
            cancelSightingSearchRequest = axios.CancelToken.source()

            $http
                .get(dataUrlQs, {
                    cancelToken: cancelSightingSearchRequest.token,
                })
                .then(function (resp) {
                    if (resp.status == 200) {
                        return resp.data
                    }
                })
                .catch(function (error) {
                    // handle error
                    if (error.__CANCEL__) {
                        console.log(
                            '<loadPlaces axios error> cancelled request'
                        )
                    } else {
                        console.log('<loadPlaces Axios error>', error)
                    }
                })
                .then(function (data) {
                    if (data.features) {
                        console.log('<loadSightings datas>', data)
                        var locationLayer = L.geoJson(data, {
                            onEachFeature: function onEachFeature(
                                feature,
                                layer
                            ) {
                                var props = feature.properties
                                var content =
                                    '<h3>' +
                                    props.specie_data.common_name_fr +
                                    '&nbsp;(<i>' +
                                    props.specie_data.sci_name +
                                    '</i>)' +
                                    '</h3><p>' +
                                    'Le ' +
                                    (props.session_data.date_start
                                        ? props.session_data.date_start
                                        : 'Non défini') +
                                    ' par ' +
                                    (props.creator
                                        ? props.creator.get_full_name
                                        : 'inconnu') +
                                    '<br /><ul><li><b>Localité:</b>&nbsp;:&nbsp;' +
                                    props.session_data.place_data.name +
                                    '</li><li><b>Dénombrement</b>:&nbsp;' +
                                    (props.total_count > 0
                                        ? props.total_count
                                        : 'Non dénombré') +
                                    '</li><li><b>Type de contact</b>: ' +
                                    (props.session_data.contact_data
                                        ? props.session_data.contact_data.descr
                                        : 'Non défini') +
                                    '</li>' +
                                    (props.breed_colo == true
                                        ? '<li><b>Colonie de reproduction</b></li>'
                                        : '') +
                                    '</ul> </p><div class="text-right">' +
                                    sightingActionButtonGroup(
                                        feature.id,
                                        'sighting'
                                    ) +
                                    '</div>'
                                layer.bindPopup(content)
                                if (feature.id) {
                                    markersById[feature.id] = layer
                                }
                            },
                            pointToLayer: function (feature, latlng) {
                                // Set a different color if location is in a metaplace
                                if (feature.properties.metaplace_data) {
                                    return L.circleMarker(
                                        latlng,
                                        style_data_metasite
                                    )
                                }
                                return L.circleMarker(latlng, style_data)
                            },
                        })
                        markers
                            .addLayer(locationLayer)
                            .addTo(searchMap)
                            .bringToFront()
                        searchMap.fitBounds(locationLayer.getBounds())
                        searchMap.invalidateSize()

                        table.rows.add(data.features).draw()
                    }
                })
                .then(function () {
                    console.log('<loadSightings spinOff>', 'spinOff')
                    displaySpinner(false)
                })
                .catch(function (error) {
                    console.log('<loadSightings Error2>', error)
                })
        }

        $('#sight_results').on('click', 'tbody td', function () {
            markerId = table.row(this).data().id
            markerPosition = markersById[markerId].getLatLng()

            if (circleMarker !== null) {
                searchMap.removeLayer(circleMarker)
            }
            circleMarker = new L.circleMarker(markerPosition, {
                radius: 20,
                color: 'red',
                fillColor: 'transparent',
            })
                .addTo(searchMap)
                .bringToBack()
            searchMap.setView(markerPosition, 15, {
                animation: true,
            })
        })

        function showValues() {
            var str = $('#searchForm input, select')
                .map(function () {
                    return $(this).val().trim() == '' ? null : this
                })
                .serialize()
            $('#results').text(str)
            dataUrlQs = `${dataUrl}?${str}`
            if (circleMarker !== null) {
                searchMap.removeLayer(circleMarker)
            }
            loadSightings()
        }

        var debounce = null
        $('#searchForm').on('change', 'input, select', function () {
            clearTimeout(debounce)
            debounce = setTimeout(showValues, 500)
        })

        showValues()
    })
}

// Sighting Form app

const countDetailBiomForm = [
    'sex',
    'age',
    'time',
    'device',
    'method',
    'manipulator',
    'validator',
    'transmitter',
    'ab',
    'd5',
    'd3',
    'pouce',
    'queue',
    'tibia',
    'pied',
    'cm3',
    'tragus',
    'poids',
    'testicule',
    'epididyme',
    'tuniq_vag',
    'gland_taille',
    'gland_coul',
    'mamelle',
    'gestation',
    'epiphyse',
    'chinspot',
    'usure_dent',
    'comment',
]

const countDetailAcousticForm = ['time', 'method', 'count', 'unit', 'comment']

const countDetailOtherForm = [
    'time',
    'method',
    'sex',
    'age',
    'count',
    'unit',
    'precision',
    'transmitter',
    'comment',
]

const countDetailTelemetryForm = [
    'time',
    'method',
    'sex',
    'age',
    'unit',
    'precision',
    'transmitter',
    'comment',
]

const CountDetailObject = () => {
    return {
        id_countdetail: null,
        method: null,
        sex: null,
        age: null,
        precision: null,
        count: null,
        unit: null,
        time: null,
        device: null,
        manipulator: null,
        validator: null,
        transmitter: null,
        ab: null,
        d5: null,
        pouce: null,
        queue: null,
        tibia: null,
        pied: null,
        cm3: null,
        tragus: null,
        poids: null,
        testicule: null,
        epididyme: null,
        tuniq_vag: null,
        gland_taille: null,
        gland_coul: null,
        mamelle: null,
        gestation: null,
        epiphyse: null,
        chinspot: null,
        usure_dent: null,
        comment: null,
    }
}

const SightingObject = (session_id) => {
    return {
        session: session_id,
        id_sighting: null,
        codesp: null,
        breed_colo: null,
        total_count: 0,
        observer: null,
        is_doubtful: false,
        comment: null,
        extra_data: null,
        countdetails: [],
        updated_by: null,
        created_by: null,
    }
}

Vue.component('transmitterSelect', {
    data() {
        return {
            options: [],
        }
    },
    props: {
        value: {
            type: Number,
        },
        session: {
            type: Number,
        },
    },
    template: `<v-select class="form-control v-select-style" label="reference" :reduce="e => e.id_transmitter" :filterable="false" :options="options" @search="onSearch" v-model="model">
    <template slot="no-options">
      Tapez pour rechercher un émetteur
    </template>
    <template v-slot:option="option">
    {{ option.reference }}
  </template>
  <template v-slot:selected-option="option">
    {{ option.reference }}
  </template>
  </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
    methods: {
        onSearch(search, loading) {
            if (search.length >= 2) {
                loading(true)
                this.search(loading, search, this)
            }
        },
        onInit() {
            if (this.model) {
                this.apiCall(this.model)
            }
        },
        apiCall(id = null) {
            $http
                .get(`/api/v1/transmitter/search?id=${id}`)
                .then((response) => {
                    this.options = response.data
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        },
        search: _debounce((loading, search, vm) => {
            $http
                .get(`/api/v1/transmitter/search?available&q=${escape(search)}`)
                .then((response) => {
                    vm.options = response.data
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        }, 350),
    },
    mounted() {
        this.onInit()
    },
})

Vue.component('specieSelect', {
    data() {
        return {
            selected: this.value,
            dict: [],
            options: [],
            disabled: true,
        }
    },
    props: {
        value: {
            type: Number,
        },
    },
    template: `
    <span>
        <v-select
            :disabled="disabled" class="form-control v-select-style"
            :options="options" :reduce="e => e.id" label="codesp" v-model="model"
            data-toggle="tooltip" data-placement="bottom"
            title="Cliquez sur l'icône d'édition pour modifier le taxon">
            <template v-slot:option="option">
                <span class="label label-default" :class="formatCodesp(option.sp_true, option.codesp)">{{option.codesp}}</span> {{option.sci_name}}
            </template>
            <template #selected-option="{ sp_true, codesp, sci_name }">
            <span class="label" :class="formatCodesp(sp_true, codesp)">{{codesp}}</span>&nbsp;{{sci_name}}
            </template>
        </v-select>
        <i class="fas fa-edit" :class="disabled ? 'text-muted':'text-warning'" @click="editState()"></i>
    </span>
    `,
    computed: {
        model: {
            get() {
                console.debug('actual', this.selected, this.disabled)
                // this.disabled = this.value != null
                if (!this.value) {
                    this.disabled = false
                }
                return this.value
            },
            set(newValue) {
                console.debug('test', newValue)
                this.$emit('input', newValue)
            },
        },
        selectedDetail: function () {
            return this.options.find((e) => e.id == this.model)
        },
    },
    methods: {
        formatCodesp(spTrue, codeSp) {
            if (codeSp != null && typeof codeSp === 'string') {
                return spTrue
                    ? 'label-success'
                    : codeSp.startsWith('0')
                    ? 'label-primary'
                    : 'label-default'
            }
        },
        editState() {
            console.debug('disable bef>', this.disabled)
            this.disabled = !this.disabled
            console.debug('disable aft>', this.disabled)
        },
        getSpecieDict() {
            this.edit = this.value == null
            if (localStorage.getItem('species') == null) {
                console.debug('No dict in localStorage')
                $http
                    .get('/api/v1/dict/specie')
                    .then((response) => {
                        console.debug(
                            'Saving downloaded dicts in localStorage',
                            response.data
                        )
                        this.options = response.data
                        localStorage.setItem(
                            'species',
                            JSON.stringify(this.options)
                        )
                    })
                    .catch((err) => {
                        console.error(err)
                    })
            } else {
                console.debug('loading species dict from localStorage')
                this.options = JSON.parse(localStorage.getItem('species'))
            }
            this.options = this.options
                .sort((a, b) => {
                    const ca = a.codesp,
                        cb = b.codesp
                    return ca < cb ? -1 : ca > cb ? 1 : 0
                })
                .sort((a, b) => Number(b.sp_true) - Number(a.sp_true))
        },
    },
    mounted() {
        this.getSpecieDict()
        // this.edit = this.value == null
        console.debug(
            'mounted edit',
            this.value,
            this.disabled,
            this.value != null
        )
    },
})

Vue.component('dictSelect', {
    data() {
        return {
            selected: this.value,
        }
    },
    props: {
        value: {
            type: Number,
        },
        options: {
            type: Array,
            required: true,
        },
    },
    template: `<v-select class="form-control v-select-style" :options="options" :reduce="e => e.id" label="code" v-model="model">
        <template v-slot:option="option">
          <span class="label label-default">{{option.code}}</span>&nbsp;{{option.short_descr ? option.short_descr : option.descr}}
        </template>
        <template #selected-option="{ code, short_descr, descr }">
          <span class="label label-default">{{code}}</span>&nbsp;{{short_descr ? short_descr : descr}}
        </template>
      </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
})

Vue.component('userSelectOne', {
    data() {
        return {
            options: [],
        }
    },
    props: {
        value: {
            type: Number,
        },
        session: {
            type: Number,
        },
    },
    template: `<v-select class="form-control v-select-style" label="get_full_name" :reduce="e => e.id" :filterable="false" :options="options" @search="onSearch" v-model="model">
    <template slot="no-options">
      Tapez pour rechercher un utilisateur
    </template>
    <template v-slot:option="option">
    {{ option.get_full_name }}
  </template>
  <template v-slot:selected-option="option">
    {{ option.get_full_name }}
  </template>
  </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
    methods: {
        onSearch(search, loading) {
            if (search.length > 2) {
                loading(true)
                this.search(loading, search, this)
            }
        },
        onInit() {
            if (this.model) {
                this.apiCall(null, this.model)
            }
        },
        apiCall(q = null, id = null) {
            const qsParams = []
            let qs
            if (id) {
                qs = `id=${id}`
            } else {
                qsParams.push(`q=${escape(q)}`)
                if (session) {
                    qs.push(`session=${this.session}`)
                }
                qs = qsParams.join('&')
            }
            console.trace('User Search QS', qs)
            $http
                .get(`/accounts/api/v1/profile/search?${qs}`)
                .then((response) => {
                    this.options = response.data
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        },
        search: _debounce((loading, search, vm) => {
            $http
                .get(`/accounts/api/v1/profile/search?q=${escape(search)}`)
                .then((response) => {
                    vm.options = response.data
                    console.debug('iotuibs', this.options)
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        }, 350),
    },
    mounted() {
        this.onInit()
    },
})

const sigthingFormApp = (id_session, type_contact) => {
    VeeValidate.extend('positive', (value) => {
        return value >= 0
    })

    const form_fields =
        type_contact == 'vm'
            ? countDetailBiomForm
            : type_contact == 'du'
            ? countDetailAcousticForm
            : countDetailOtherForm

    VeeValidate.extend('required', {
        validate(value) {
            return {
                required: true,
                valid: ['', null, undefined].indexOf(value) === -1,
            }
        },
        computesRequired: true,
    })

    VeeValidate.configure({
        classes: true,
        classNames: {
            valid: 'has-success',
            invalid: 'has-error',
            dirty: 'has-warning', // multiple classes per flag!
        },
    })

    Vue.component('ValidationProvider', VeeValidate.ValidationProvider)
    Vue.component('ValidationObserver', VeeValidate.ValidationObserver)
    Vue.component('v-select', VueSelect.VueSelect)

    return {
        el: '#sessionSightingEdit',
        delimiters: ['[[', ']]'],
        data() {
            return {
                data: [],
                dicts: {},
                form_fields:
                    type_contact == 'vm'
                        ? countDetailBiomForm
                        : type_contact == 'du'
                        ? countDetailAcousticForm
                        : countDetailOtherForm,
                url: sightingDataListUrl,
                loadingMessage: true,
                initialDataLoaded: false,
                postLoad: false,
                dictsLoaded: false,
                newSighting: SightingObject(id_session),
                newCountDetail: CountDetailObject(),
                hover: true,
                isVisible: false,
                showRawData: false,
                selectedItem: 0,
                typeContact: type_contact,
                moreSightingForm: false,
                moreCountDetailForm: false,
                messages: [],
                countDetailSelectedItem: -1,
                postInProgress: 0,
                sightingToDelete: -1,
            }
        },
        methods: {
            findCondition(dict, code) {
                return dict.find((e) => e.code === code)
            },
            displayCountDetailForm(icd) {
                this.countDetailSelectedItem = icd
                // if (this.countDetailSelectedItem > -1) {
                //     this.countDetailSelectedItem = -1
                // } else {
                //     this.countDetailSelectedItem = icd
                // }
            },
            selectItem(i) {
                this.selectedItem = i
                this.countDetailSelectedItem = -1
            },
            showCountDetail(item) {
                item.isVisible = !item.isVisible
                this.$forceUpdate()
            },
            getTaxa(specieId) {
                return this.dicts.Specie.find((s) => s.id === specieId)?.code
            },
            hideLoadingMessage() {
                return !(this.initialDataLoaded && this.dictsLoaded)
            },
            addNewSighting() {
                if (!this.data.find((e) => e.codesp == null)) {
                    const newSightingObject = JSON.parse(
                        JSON.stringify(this.newSighting)
                    )
                    const newCountDetailObject = { ...this.newCountDetail }
                    const allCountDetails = []
                    this.data.forEach((es) =>
                        allCountDetails.push(...es.countdetails)
                    )
                    const lastCountDetail =
                        allCountDetails[allCountDetails.length - 1]
                    if (lastCountDetail) {
                        newCountDetailObject.time = lastCountDetail.time
                        newCountDetailObject.method = lastCountDetail.method
                        newCountDetailObject.precision =
                            lastCountDetail.precision
                        newCountDetailObject.unit = lastCountDetail.unit
                        if (type_contact == 'vm') {
                            newCountDetailObject.count = 1
                        }
                    }
                    newSightingObject.countdetails.push(newCountDetailObject)
                    if (type_contact == 'du') {
                        newSightingObject.total_count = 1
                    }
                    this.data.push(newSightingObject)
                    this.selectedItem = this.data.length - 1
                    this.countDetailSelectedItem = 0
                } else {
                    alert(
                        'Vous ne pouvez pas entrez d autre taxon si tous ne sont pas complétés'
                    )
                }
            },
            // refactorForSelect(dict, contact) {
            //     const dict = this.filteredDict(dict, contact)
            //     const options = dict.map(x => {label: x.})
            // },
            addNewCountDetail(is) {
                const newCountDetail = { ...this.newCountDetail }
                console.debug(
                    `<addNewCountDetail> ${this.data[is].countdetails.length} countdetails items`
                )
                if (this.data[is].countdetails.length > 0) {
                    console.debug('<addNewCountDetail> has countdetails items')
                    const lastCountDetail = this.data[is].countdetails[
                        this.data[is].countdetails.length - 1
                    ]
                    // const lastCountDetail = this.data[is].countdetails.slice(-1)
                    console.debug(
                        'newObject before',
                        lastCountDetail,
                        newCountDetail
                    )
                    newCountDetail.time = lastCountDetail.time
                    newCountDetail.method = lastCountDetail.method
                    newCountDetail.precision = lastCountDetail.precision
                    newCountDetail.unit = lastCountDetail.unit
                    if (type_contact == 'vm') {
                        newCountDetail.count = 1
                    }
                    console.log('newObject after', newCountDetail)
                    console.log('countdetails', this.data[is].countdetails)
                }
                this.data[is].countdetails.push(newCountDetail)
                this.countDetailSelectedItem =
                    this.data[is].countdetails.length - 1
            },
            delCountDetail(is, icd) {
                this.data[is].countdetails.splice(icd, 1)
            },
            getSpecieValue(isp, field) {
                specieList = this.dicts.Specie
                return isp ? specieList.find((r) => r.id === isp)[field] : null
            },
            getSpClass(isp) {
                specieList = this.dicts.Specie
                return isp
                    ? specieList.find((r) => r.id === isp).sci_name
                    : null
            },
            postItem(d) {
                console.log('PostInProgress', this.postInProgress)
                console.debug('POST datas', d)
                const sightingUpdateUrl = d.id_sighting
                    ? `/api/v1/observation/${d.id_sighting}/update`
                    : `/api/v1/session/${id_session}/observation/add`
                const $httpMethod = d.id_sighting ? $http.put : $http.post

                return $httpMethod(sightingUpdateUrl, d)
                    .then((resp) => {
                        ++this.postInProgress
                        return resp
                    })
                    .catch((err) => {
                        console.error(err)
                        this.messages.push({
                            type: 'danger',
                            message: err.response.data,
                        })
                    })
            },
            postSighting(d) {
                console.log('PostInProgress', this.postInProgress)
                console.debug('POST datas', d)
                const sightingUpdateUrl = d.id_sighting
                    ? `/api/v1/observation/${d.id_sighting}/update`
                    : `/api/v1/session/${id_session}/observation/add`
                const $httpMethod = d.id_sighting ? $http.put : $http.post

                return $httpMethod(sightingUpdateUrl, d)
                    .then((response) => {
                        console.debug('response', response)
                        ++this.postInProgress
                    })
                    .catch((err) => {
                        console.error(err)
                        this.messages.push({
                            type: 'danger',
                            message: err.response.data,
                        })
                    })
            },
            deleteSightingShowModal(i) {
                this.sightingToDelete = i
            },
            delSighting(i) {
                id = this.data[i].id_sighting
                if (id) {
                    $http
                        .delete(`/api/v1/observation/${id}/delete`)
                        .then((r) => {
                            console.log('DELETE', r)
                            this.data.splice(i, 1)
                        })
                } else {
                    this.data.splice(i, 1)
                }
                this.sightingToDelete = -1
            },
            postOneData(i) {
                this.postSighting(this.data[i])
            },
            postData() {
                const callback = () => {
                    this.getData()
                }
                this.messages.length = 0
                this.messages.push({
                    type: 'info',
                    message: `Saving data in progress`,
                })
                this.postInProgress = 0
                axios
                    .all(this.data.map((item) => this.postItem(item)))
                    .then(
                        axios.spread((...responses) => {
                            console.log(responses)
                            responses.forEach((item, index) =>
                                console.debug('TEST', item, index)
                            )
                            if (
                                responses.every(
                                    (item) =>
                                        item?.status == 200 ||
                                        item?.status == 201
                                )
                            ) {
                                this.messages.length = 0
                                this.messages.push({
                                    type: 'success',
                                    message: 'Données enregistrées avec succès',
                                })

                                this.getData()
                            }
                            this.postInProgress = 0
                        })
                    )
                    .catch((err) => {
                        this.messages.length = 0
                        console.debug('ERROR', err)
                        this.messages.push({ type: 'danger', message: err })
                    })
            },
            filteredDict(dict, contact) {
                contact = contact || null
                return contact ? dict.filter((e) => e.contact == contact) : dict
            },
            selectedValueHtml(dict, value) {
                // console.debug(
                //     'selectedValue',
                //     dict.find((e) => e.id == value)
                // )
                const r = dict.find((e) => e.id == value)
                return value
                    ? `<span class="label label-default">${r?.code}</span> ${
                          r?.short_decr ? r.short_descr : r?.descr
                      }`
                    : '-'
            },
            selectedCode(dict, value, code) {
                return dict.find((e) => e.id == value)?.code == code
            },
            taxaSelectOptions(dict, contact) {
                return
            },
            getDictSpecie() {},
            getDicts() {
                this.dicts = JSON.parse(localStorage.getItem('dicts'))
                this.dictSpecies = JSON.parse(
                    localStorage.getItem('dictSpecies')
                )
            },
            getAutocompleteData(obj) {
                this.data.user = obj
            },
            sumCountDetails(is) {
                const CountSum = (a, c) => a + parseInt(c.count)
                // const notNullData = this.data[is].countdetails.filter(e => e.count !== null)
                return this.data[is].countdetails
                    .filter((e) => e.count !== null)
                    .reduce(CountSum, 0)
            },
            getData() {
                if (Object.entries(this.dicts).length === 0) {
                    this.getDicts()
                }
                $http
                    .get(this.url)
                    .then((response) => {
                        this.data = response.data?.results
                        if (this.data.length == 0) {
                            this.addNewSighting()
                        }
                        console.debug('this.data', this.data)
                    })
                    .then(() => {
                        this.initialDataLoaded = true
                    })
                    .catch((err) => {
                        console.error(err)
                        this.message.push({ type: 'error', message: err })
                    })
            },
            dupSpecies() {
                this.messages.length = 0
                const cleanArray = [...new Set(this.species)]
                let duplicates = [...this.species]
                cleanArray.forEach((item) => {
                    const i = duplicates.indexOf(item)
                    duplicates = duplicates
                        .slice(0, i)
                        .concat(duplicates.slice(i + 1, duplicates.length))
                })
                // console.debug('<dupSpecies>', duplicates) //[ 1, 5 ]
                if (duplicates.length > 0) {
                    this.messages.push({
                        type: 'danger',
                        message: `Espèce(s) ${duplicates.map(
                            (e) => `${this.getSpecieValue(e, 'sci_name')}`
                        )} en double`,
                    })
                }
            },
        },
        computed: {
            species() {
                return this.data.map((e) => e.codesp)
            },
        },
        watch: {
            data: {
                handler() {
                    if (type_contact != 'du') {
                        this.data.forEach((e, i) => {
                            if (
                                e.total_count != null &&
                                e.countdetails.length == 0
                            ) {
                            } else {
                                e.total_count = this.sumCountDetails(i)
                            }
                        })
                    }
                    this.dupSpecies()
                },
                deep: true,
            },
        },
        mounted() {
            this.getDicts()
            this.getData()
            this.messages.push()
        },
        // options
    }
}

Vue.component('session-group-item', {
    props: ['data'],
    template: `<div class="list-group-item">
    <h3 class="list-group-item-heading">
        <span class="pull-right label label-info label-lg">{{data.properties.sessions.length}} sessions</span>
        <i class="fa fa-map-marker fa-fw"></i><a :href="'/place/'+data.id+'/detail'" target="_blank">{{data.properties.name}}</a>
    </h3>
    <div class="list-group">
      <div v-for="(se, ie) in data.properties.sessions" class="list-group-item">
        <h4 class="list-group-item-heading">
          <span class="pull-right label label-default label-lg">{{se.contact.descr}}</span>
          <span class="text-info"><a :href="'/session/'+se.id_session+'/detail'">{{se.date_start}}</a></span>
        </h4>
        <p class="list-group-item-text">
          <dl class="dl-horizontal">
            <dt>Observateur principal</dt>
            <dd>
              {{se.main_observer?.get_full_name}}</dd>
            <dt>Observations</dt>
            <dd>
              <ul class="list-unstyled">
                <li v-for='(o, io) in se.observations' :class="{'text-muted':!o.codesp.sp_true}">
                <small><span v-show="o.breed_colo"  class="fa-stack">
                <i class="fas fa-circle fa-stack-2x text-success"></i>
                <i class="fas fa-baby fa-stack-1x fa-inverse"></i>
              </span></small>

                <!--<i v-show="o.breed_colo" class="fas fa-baby fa-fw text-success"></i>-->
                  <strong>{{o.codesp.common_name_fr}}</strong>
                  (<i>{{o.codesp.sci_name}}</i>)
                  : {{o.total_count ? o.total_count: '-'}}

                </li>
              </ul>
            </dd>
          </dl>
        </p>
      </div>
    </div>
  </div>`,
})

Vue.component('pie-chart', {
    extends: VueChartJs.Pie,
    props: ['chartData', 'options'],
    mounted() {
        this.renderChart(this.chartData, this.options)
    },
})

const studyDataApp = (pk) => {
    // Vue.component('pie-chart', {
    //     extends: Pie,
    //     data() {
    //         return {
    //             options: {
    //                 parsing: {
    //                     xAxisKey: 'label',
    //                     yAxisKey: 'value',
    //                 },
    //             },
    //         }
    //     },
    //     props: {
    //         chartData: {
    //             type: Object,
    //             default: null,
    //         },
    //     },
    //     mounted() {
    //         this.renderChart(this.chartData, this.options)
    //     },
    // })

    return {
        el: '#StudyData',
        delimiters: ['[[', ']]'],
        data() {
            return {
                pk: pk,
                msg: 'Test',
                placeData: [],
                sessionData: [],
                sightingData: [],
                dataSource: 'stats',

                //sessions: [],
                loading: true,
                contactChartDict: {
                    vv: { label: 'Contact visuel', color: '#e89bda' },
                    du: { label: 'Contact acoustique', color: '#7ed6a0' },
                    vm: { label: 'Vu en main', color: '#c6c63d' },
                    te: { label: 'Contact télémétrique', color: '#517eef' },
                    nc: { label: 'Non communiqué', color: '#aaaaaa' },
                },
                contactChartOptions: {
                    options: {
                        parsing: {
                            xAxisKey: 'label',
                            yAxisKey: 'value',
                        },
                    },
                },
                dicts: {},
                dictSpecies: {},
            }
        },
        methods: {
            getSightings() {
                $http
                    .get(`/api/v1/study/${pk}/datas?format=json`)
                    .then((r) => {
                        console.debug(r)
                        this.sightingData = r.data.results.features
                        //this.countSessions()
                        this.loading = false
                    })
                    .catch((error) => console.error(error))
            },
            getDicts() {
                this.dicts = JSON.parse(localStorage.getItem('dicts'))
                this.dictSpecies = JSON.parse(
                    localStorage.getItem('dictSpecies')
                )
            },
            countPlaces() {
                return this.sightingData.length
            },
            // countSessions2() {
            //     this.sessions = []
            //     console.error('TypeOf Fata', typeof(this.sightingData));
            //     console.error('Data', this.sightingData);
            //     this.sessions = [...this.sightingData.forEach(e => this.sessions.push(...e.properties.sessions))]
            // }
        },
        computed: {
            sessions() {
                return Array.prototype.concat.apply(
                    [],
                    this.sightingData.map((e) => e.properties.sessions)
                )
            },
            minDate() {
                //return this.sessions.map(a=> e.date_start).reduce((a, b) => { return a < b ? a : b; })
                return new Date(
                    Math.min(
                        ...this.sessions.map((e) => new Date(e.date_start))
                    )
                ).toLocaleDateString()
            },
            maxDate() {
                return new Date(
                    Math.max(
                        ...this.sessions.map(
                            (e) =>
                                new Date(e.date_end ? e.date_end : e.date_start)
                        )
                    )
                ).toLocaleDateString()
            },
            contacts() {
                const contacts = [
                    ...new Set(this.sessions.map((e) => e.contact.code)),
                ]
                console.log('contacts', contacts)
                const data = []
                contacts.forEach((e) => {
                    data.push({
                        code: e,
                        value: this.sessions.filter(
                            (se) => se.contact.code == e
                        ).length,
                    })
                })
                return data
            },
            taxa() {
                const sightings = this.sessions
                    .map((e) => e.observations)
                    .flat()
                console.log(sightings)
                const listTaxa = [
                    ...new Set(sightings.map((e) => e.codesp.codesp)),
                ]
                const taxa = []
                listTaxa.forEach((e) => {
                    taxa.push({
                        code: e,
                        common_name: this.dictSpecies.find(
                            (sp) => sp.codesp == e
                        ).common_name_fr,
                        sci_name: this.dictSpecies.find((sp) => sp.codesp == e)
                            .sci_name,
                        sp_true: this.dictSpecies.find((sp) => sp.codesp == e)
                            .sp_true,
                        value: sightings.filter(
                            (se) => se.codesp.codesp == e && se.total_count > 0
                        ).length,
                    })
                })
                return taxa.sort((a, b) => b.value - a.value)
            },
            main_observers() {
                const main_observers = this.sessions
                    .map((e) => e.main_observer)
                    .flat()
                return [...new Set(main_observers.map((e) => e.get_full_name))]
            },
            chartData() {
                return {
                    labels: [
                        ...this.contacts.map(
                            (e) => this.contactChartDict[e.code].label
                        ),
                    ],
                    datasets: [
                        {
                            label: 'ds1',
                            backgroundColor: [
                                ...this.contacts.map(
                                    (e) =>
                                        this.contactChartDict[e.code].color ||
                                        '#ef517b'
                                ),
                            ],
                            data: [...this.contacts.map((e) => e.value)],
                        },
                    ],
                }
            },
        },
        mounted() {
            this.getSightings(), this.getDicts()
        },
    }
}

const strNormalize = (str) => {
    return str
        .toLowerCase()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
}

const studyListApp = (userId) => {
    return {
        el: '#studyList',
        delimiters: ['[[', ']]'],
        data() {
            return {
                loading: true,
                fullStudyList: [],
                studyList: [],
                search: {},
            }
        },
        methods: {
            getStudies() {
                $http
                    .get(`/api/v1/study/list?format=json`)
                    .then((r) => {
                        console.debug(r)
                        this.fullStudyList = r.data.results
                        this.studyList = [...this.fullStudyList]
                        this.loading = false
                    })
                    .catch((error) => console.error(error))
            },
            filterStudies() {
                let filteredStudies = [...this.fullStudyList]
                if (this.search.name) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.name &&
                            strNormalize(e.name).includes(
                                strNormalize(this.search.name)
                            )
                    )
                }
                if (this.search.year) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.year && e.year.includes(this.search.year)
                    )
                }
                if (this.search.myStudies) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.project_manager && e.project_manager?.id == userId
                    )
                }
                if (this.search.manager) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.project_manager &&
                            strNormalize(
                                e.project_manager?.get_full_name
                            ).includes(strNormalize(this.search.manager))
                    )
                }
                if (this.search.type_espace) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.type_espace &&
                            strNormalize(e.type_espace).includes(
                                strNormalize(this.search.type_espace)
                            )
                    )
                }
                if (this.search.type_etude) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.type_etude &&
                            strNormalize(e.type_etude).includes(
                                strNormalize(this.search.type_etude)
                            )
                    )
                }
                if (this.search.public_funding) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.public_funding
                    )
                }
                if (this.search.public_report) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.public_report
                    )
                }
                if (this.search.public_raw_data) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.public_raw_data
                    )
                }
                if (this.search.confidential) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.confidential
                    )
                }
                this.studyList = filteredStudies
            },
        },
        watch: {
            search: {
                handler(val, oldVal) {
                    console.log('book list changed')
                    this.filterStudies()
                },
                deep: true,
            },

            //(search, oldSearch) {
            //    console.log(search)
            //    this.filterStudies()
            //}
        },
        mounted() {
            this.getStudies()
        },
    }
}
