import logging
import os

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import connection
from django.views.generic import TemplateView

from .functions import version
from .mixins import AdminRequiredMixin

logger = logging.getLogger(__name__)


def db_datas():
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT pg_size_pretty( pg_database_size( current_database() ) ), version(), postgis_version()"
        )
        row = cursor.fetchone()
        logger.debug(row)
        data = {}
        data["database_size"] = row[0]
        data["database_pg_version"] = row[1]
        data["database_postgis_version"] = row[2]

    return data


class UnauthorizedModify(LoginRequiredMixin, TemplateView):
    template_name = "unauthorized.html"

    def get_context_data(self, **kwargs):
        context = super(UnauthorizedModify, self).get_context_data(**kwargs)
        context["message"] = "Vous n'êtes pas autorisé à modifier ou supprimer cette donnée"
        return context


class UnauthorizedView(LoginRequiredMixin, TemplateView):
    template_name = "unauthorized.html"

    def get_context_data(self, **kwargs):
        context = super(UnauthorizedView, self).get_context_data(**kwargs)
        context["message"] = "Vous n'êtes pas autorisé à voir cette donnée"
        return context


class SystemInformation(AdminRequiredMixin, TemplateView):
    template_name = "sysinfo.html"

    def get_context_data(self, **kwargs):
        context = super(SystemInformation, self).get_context_data(**kwargs)
        server_info = {}
        server_info["dbchiro_version"] = version
        server_info["hosting_server"] = os.uname()
        sysinfo = {**server_info, **db_datas()}
        context["sysinfo"] = sysinfo
        return context
